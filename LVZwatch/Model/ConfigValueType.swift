//
//  ConfigValueType.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 18.11.2020.
//

import Foundation

enum ConfigValueType : String {
    case bool
    case signedByte
    case signed16Bit
    case signed32Bit
    case signed64Bit
    case utf8String
    case signedFixedPoint
}
