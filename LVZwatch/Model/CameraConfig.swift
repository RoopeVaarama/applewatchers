//
//  CameraConfig.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 17.11.2020.
//

import Foundation

/// Contains configuration settings for all camera value related features
enum CameraConfig : String {
    case startRecord
    case stopRecord
    case record
    case autoFocus
    case ISO
    case absoluteFocus
    case apertureFstop
    case apertureNormalized
    case autoAperture
    case contZoom
    case levelZoom
    case shutterSpeed
    case manualWhiteBalance
    case pureWhiteBalance
    case tint
    case preset
    case presetName
    case firstPreset
    case secondPreset
    case thirdPreset
    case fourthPreset
}
