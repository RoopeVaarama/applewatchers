//
//  RecordingStatus.swift
//  AppleWatchers
//
//  Created by iosdev on 25.11.2020.
//

import Foundation

class RecordingStatus {
    
    static let shared = RecordingStatus()
    private init() {
        print("recordingstatus initialized")
    }
    
    var isRecording: Bool?
    
}
