//
//  ConfigOperationType.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 18.11.2020.
//

import Foundation

enum ConfigOperationType : String {
    case assign
    case offset
}
