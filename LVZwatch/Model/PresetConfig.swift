//
//  PresetConfig.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 7.12.2020.
//

import Foundation

enum PresetConfig : String {
    case preset1Values
    case preset2Values
    case preset3Values
    case preset4Values
}
