//
//  DeviceTableViewCell.swift
//  AppleWatchers
//
//  Created by Roope Vaarama on 5.11.2020.
//

import Foundation
import UIKit

class DeviceTableViewCell: UITableViewCell {
    //MARK: Properties
    @IBOutlet var deviceTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
