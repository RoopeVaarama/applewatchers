//
//  ViewController.swift
//  AppleWatchers
//
//  Created by Roope Vaarama on 27.10.2020.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, UITableViewDelegate {
    
    // MARK: - Properties
    var centralManager: CBCentralManager!
    var myPeripheral: CBPeripheral!
    let cameraServiceUUID = "291D567A-6D75-11E6-8B77-86F30CA893D3"
    var btON: Bool = false
    var peripherals = Array<CBPeripheral>()
    @IBOutlet weak var deviceTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deviceTableView.dataSource = self
        deviceTableView.delegate = self
        
        UserDefaults.standard.setValue("400", forKey: "ISO")
        
        /// Start bluetooth
        centralManager = CBCentralManager(delegate: self, queue: nil)
        
    }

    //MARK: Buttons
    @IBAction func Button(_ sender: Any) {
        if(btON){
        clearTableView()
        print("Scanning for devices")
        let services = [CBUUID(string: cameraServiceUUID)]
        centralManager.scanForPeripherals(withServices: services, options: nil)
        } else {
            //showToast(message: "Bluetooth is not on", seconds: 2.0)
            self.performSegue(withIdentifier: "testSegue", sender: nil)
        }
    }
    
    
    //MARK: TableView clearing function
    func clearTableView(){
        print("Clearing the tableview")
        peripherals.removeAll()
        deviceTableView.reloadData()
    }
    // MARK: - CentralManager (Bluetooth functions)
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            print("BLE powered on")
            btON = true
        }
        else {
            print("Something wrong with BLE")
            clearTableView()
            dismiss(animated: true, completion: nil)
            //Not on, but can have different issues
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        clearTableView()
        dismiss(animated: true, completion: nil)
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print(peripherals)
        peripherals.append(peripheral)
        deviceTableView.reloadData()
    }
    
    
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Did connect")
        //IF SHOWTOAST IS CALLED THE SEGUE DOESN'T WORk
        //showToast(message: "Device did connect", seconds: 0)
        ProtocolPacketLibrary.sharedLibrary.peripheral = myPeripheral
        self.performSegue(withIdentifier: "testSegue", sender: nil)
    }
}

//MARK: Extension for ViewController tableView
extension ViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceTableViewCell", for: indexPath) as? DeviceTableViewCell else {
            fatalError("Cell is wrong")
        }
        let peripheral = peripherals[indexPath.row]
        cell.deviceTitle?.text = peripheral.name
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        centralManager.stopScan()
        self.myPeripheral = peripherals[indexPath.row]
        centralManager.connect(peripherals[indexPath.row], options: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peripherals.count
    }
}

// MARK: - Extension for ViewController for toast messages
extension ViewController {
  func showToast(message: String, seconds: Double) {
    let alert = UIAlertController(title: nil, message: message,
      preferredStyle: .alert)
    alert.view.backgroundColor = UIColor.black
    alert.view.alpha = 0.6
    alert.view.layer.cornerRadius = 15
    present(alert, animated: true)
    DispatchQueue.main.asyncAfter(
      deadline: DispatchTime.now() + seconds) {
        alert.dismiss(animated: true)
    }
  }
}

