//
//  DeviceTableViewController.swift
//  AppleWatchers
//
//  Created by Lauri Nissinen on 3.11.2020.
//

//import UIKit

/*class  MainTableViewController: UITableViewController {
     
    var devices = [Device]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("View loaded")
        
        loadSampleDevices()
        
        //loadDevicesFromBT()
        
    }
    
    private func loadDevicesFromBT(){
        //Load devicelist and set it to devices array
    }
    
    private func loadSampleDevices() {
        
        guard let device1 = Device(name: "Iphone", id: 1, ss: 10) else {
            fatalError("Unable to instantiate device1")
        }
        
        guard let device2 = Device(name: "Fitbit", id: 2, ss: 35) else {
            fatalError("Unable to instantiate device2")
        }
        
        guard let device3 = Device(name: "Android Phone", id: 3, ss: 74) else {
            fatalError("Unable to instantiate device3")
        }
        
        devices += [device1, device2, device3]
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "DeviceTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MainTableViewCell  else {
            fatalError("The dequeued cell is not an instance of DeviceTableViewCell.")
        }
        let device = devices[indexPath.row]
        
        cell.nameLabel.text = device.name
        
        return cell
    }
}*/
