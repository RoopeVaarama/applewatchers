//
//  DeviceViewController.swift
//  AppleWatchers
//
//  Created by Lauri Nissinen on 16.11.2020.
//

import UIKit
import CoreBluetooth
import WatchConnectivity

class DeviceViewController: UIViewController, CBPeripheralDelegate {

    // MARK: Properties
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var apertureSlider: UISlider!
    @IBOutlet weak var isoSlider: UISlider!
    @IBOutlet weak var shutterSlider: UISlider!
    @IBOutlet weak var wbSlider: UISlider!
    @IBOutlet weak var focusSlider: UISlider!
    @IBOutlet weak var zoomSlider: UISlider!
    @IBOutlet weak var tintSlider: UISlider!
    @IBOutlet weak var apertureValueLabel: UILabel!
    @IBOutlet weak var isoValueLabel: UILabel!
    @IBOutlet weak var shutterValueLabel: UILabel!
    @IBOutlet weak var tintValueLabel: UILabel!
    @IBOutlet weak var wbValueLabel: UILabel!
    @IBOutlet weak var focusValueLabel: UILabel!
    @IBOutlet weak var zoomValueLabel: UILabel!
    @IBOutlet var preset1Button: UIButton!
    @IBOutlet var preset2Button: UIButton!
    @IBOutlet var preset3Button: UIButton!
    @IBOutlet var preset4Button: UIButton!
    var outgoingWriteCharacteristic: CBCharacteristic?
    var cameraStatusCharacteristic : CBCharacteristic?
    var session : WCSession?
    let byteIntepreter : ByteIntepreter = ByteIntepreter()
    let shutterSpeedValueConverter : ShutterSpeedValueConverter = ShutterSpeedValueConverter()
    let isoValueConverter : ISOValueConverter = ISOValueConverter()
    let apertureValueConverter : ApertureValueConverter = ApertureValueConverter()
    let cameraServiceUUID = "291D567A-6D75-11E6-8B77-86F30CA893D3"
    let cameraStatusCharacteristicUUID = "7FE8691D-95DC-4FC5-8ABD-CA74339B51B9"
    let incomingCameraControlCharacteristicUUID = "B864E140-76A0-416A-BF30-5876504537D9"
    let outgoingCameraControlCharacteristicUUID = "5DD3465F-1AEE-4299-8493-D2ECA2F8E1BB"
    var myPeripheral : CBPeripheral?
    let defaults = UserDefaults.standard
    var preset1ButtonIsEnabled = false
    var preset2ButtonIsEnabled = false
    var preset3ButtonIsEnabled = false
    var preset4ButtonIsEnabled = false
    var presetName = ""
    var presetToSave = "0"
    var wbIsSaving = false
    var tintIsSaving = false
    var isoIsSaving = false
    var shutterIsSaving = false
    var apertureIsSaving = false
    var focusIsSaving = false
    var zoomIsSaving = false
    var centralManager: CBCentralManager!
    
    /// Observer variables. Every value has two observer variables: one for the actual value and one for the slider's value
    var shutterSpeedValue : Int32 = 0 {
        didSet {
            print("shutter speed changed \(shutterSpeedValue)")
            let newSliderValue = shutterSpeedValueConverter.convertShutterSpeedToSliderValue(shutterSpeed: shutterSpeedValue)
            if (newSliderValue != nil) {
                shutterValueLabel.text = "1/\(shutterSpeedValue)"
                shutterSlider.value = newSliderValue!
            }
        }
    }
    var shutterSliderValue : Float = 0 {
        didSet {
            let futureShutterValue  = shutterSpeedValueConverter.convertSliderValueToShutterSpeed(sliderValue: shutterSliderValue)
            
            if (futureShutterValue != shutterSpeedValue && futureShutterValue != nil) {
                shutterSpeedValue = futureShutterValue!
            }
        }
    }
    var isoValue : Int32 = 0 {
        didSet {
            let newSliderValue = isoValueConverter.convertISOToSliderValue(isoValue: isoValue)
            if (newSliderValue != nil) {
                isoValueLabel.text = String(isoValue)
                isoSlider.value = newSliderValue!
            }
        }
    }
    var isoSliderValue : Float = 0 {
        didSet {
            let futureIsoValue = isoValueConverter.convertSliderValueToISO(sliderValue: isoSliderValue)
            
            if (futureIsoValue != isoValue && futureIsoValue != nil) {
                isoValue = futureIsoValue!
            }
        }
    }
    var focusValue : Double = 0.0 {
        didSet {
            print("focusvalue changed \(focusValue)")
            let focSliderValue = Float(focusValue * 1000)
            focusSlider.value = focSliderValue
            focusValueLabel.text = String(format: "%.0f", focSliderValue)
        }
    }
    var focusSliderValue : Float = 0 {
        didSet {
            var futureFocus : Double = Double(focusSliderValue / 1000)
            futureFocus = Double(round(1000*futureFocus)/1000)
            if (futureFocus != focusValue) {
                focusValue = futureFocus
            }
        }
    }
    var apertureValue : Double = 0.0 {
        didSet {
            print(apertureValue)
            let fNumber = apertureValueConverter.convertApertureValueToFNumber(apertureValue: apertureValue)
            apertureSlider.value = Float(apertureValue * 10)
            apertureValueLabel.text = fNumber
        }
    }
    var apertureSliderValue : Float = 0 {
        didSet {
            print("aperture slider val \(apertureSliderValue)")
            let futureApertureValue = Double(apertureSlider.value) / 10
            if (futureApertureValue != apertureValue) {
                apertureValue = futureApertureValue
            }
        }
    }
    var wbValue : Int16 = 0 {
        didSet {
            let sliderVal = (Float(wbValue) - 2500) / 50
            print(sliderVal)
            wbSlider.value = sliderVal
            wbValueLabel.text = "\(wbValue)K"
        }
    }
    var wbValueSlider : Float = 0 {
        didSet {
            let futureWBValue = Int16((wbValueSlider * 50) + 2500)
            
            if (futureWBValue != wbValue) {
                wbValue = futureWBValue
            }
        }
    }
    var tintValue : Int16 = 0 {
        didSet {
            print("Tint value: \(tintValue)")
            tintSlider.value = Float(tintValue)
            tintValueLabel.text = String(tintValue)
        }
    }
    var tintValueSlider : Float = 0 {
        didSet {
            let futureTintValue = Int16(tintValueSlider)
            if (futureTintValue != tintValue) {
                tintValue = futureTintValue
            }
        }
    }
    
    var zoomValue : Double = 0.0 {
        didSet {
            print("zoomvalue changed \(zoomValue)")
            let zoomSliderValue = Float(zoomValue * 1000)
            zoomSlider.value = zoomSliderValue
            zoomValueLabel.text = String(format: "%.0f", zoomSliderValue)
        }
    }
    var zoomSliderValue : Float = 0 {
        didSet {
            var futureZoom : Double = Double(zoomSliderValue / 1000)
            futureZoom = Double(round(100000*futureZoom)/100000)
            if (futureZoom != zoomValue) {
                zoomValue = futureZoom
            }
        }
    }
    
    var zoomSpeedValue : Double = 0.0 {
        didSet {
            //let zoomSpeedSliderValue = Float(zoomSpeedValue * 1000)
        }
    }
    var zoomSpeedSliderValue : Float = 0 {
        didSet {
            let futureZoomSpeed = Double(zoomSpeedSliderValue) / 1000
            if (futureZoomSpeed != zoomSpeedValue) {
                zoomSpeedValue = futureZoomSpeed
            }
        }
    }
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus) {
                recordButton.isSelected = true
                recordButton.tintColor = UIColor.red
                ProtocolPacketLibrary.sharedLibrary.startRecordVideo()
            } else {
                recordButton.isSelected = false
                recordButton.tintColor = UIColor.lightGray
                ProtocolPacketLibrary.sharedLibrary.stopRecordVideo()
            }
        }
    }
    
    // MARK: Basic functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Get peripheral from library and discover services
        myPeripheral = ProtocolPacketLibrary.sharedLibrary.peripheral
        let incomingUUID = [CBUUID(string: cameraServiceUUID)]
        myPeripheral?.delegate = self
        myPeripheral?.discoverServices(incomingUUID)
        
        /// Setup session with watch
        self.configureWatchKitSession()
        
        setAllLabels()
        
        ///Setting sliders to their proper didEndSliding functions
        self.wbSlider.addTarget(self, action: #selector(DeviceViewController.wbSliderDidEndSliding), for: ([.touchUpInside,.touchUpOutside]))
        self.shutterSlider.addTarget(self, action: #selector(DeviceViewController.shutterSliderDidEndSliding), for: ([.touchUpInside,.touchUpOutside]))
        self.focusSlider.addTarget(self, action: #selector(DeviceViewController.focusSliderDidEndSliding), for: ([.touchUpInside,.touchUpOutside]))
        self.isoSlider.addTarget(self, action: #selector(DeviceViewController.isoSliderDidEndSliding), for: ([.touchUpInside,.touchUpOutside]))
        self.apertureSlider.addTarget(self, action: #selector(DeviceViewController.apertureSliderDidEndSliding), for: ([.touchUpInside,.touchUpOutside]))
        self.zoomSlider.addTarget(self, action: #selector(DeviceViewController.zoomSliderDidEndSliding), for: ([.touchUpInside,.touchUpOutside]))
        self.tintSlider.addTarget(self, action: #selector(DeviceViewController.tintSliderDidEndSliding), for: ([.touchUpInside,.touchUpOutside]))
        
        ///Enabling preset buttons if preset is saved and getting the custom name for the button if there is one
        preset1ButtonIsEnabled = defaults.bool(forKey: "1")
        if(preset1ButtonIsEnabled){
            let savedname = defaults.string(forKey: "customName1")
            if(savedname != nil){
                preset1Button.setTitle(savedname, for: .normal)
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.firstPreset.rawValue : [CameraConfig.presetName.rawValue : savedname]], replyHandler: nil, errorHandler: nil)
                    }
            }
        }else{
            preset1Button.setTitle("+", for: .normal)
        }
        preset2ButtonIsEnabled = defaults.bool(forKey: "2")
        if(preset2ButtonIsEnabled){
            let savedname = defaults.string(forKey: "customName2")
            if(savedname != nil){
                preset2Button.setTitle(savedname, for: .normal)
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.secondPreset.rawValue : [CameraConfig.presetName.rawValue : savedname]], replyHandler: nil, errorHandler: nil)
                    }
            }
        }else{
            preset2Button.setTitle("+", for: .normal)
        }
        preset3ButtonIsEnabled = defaults.bool(forKey: "3")
        if(preset3ButtonIsEnabled){
            let savedname = defaults.string(forKey: "customName3")
            if(savedname != nil){
                preset3Button.setTitle(savedname, for: .normal)
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.thirdPreset.rawValue : [CameraConfig.presetName.rawValue : savedname]], replyHandler: nil, errorHandler: nil)
                    }
                }
        } else{
            preset3Button.setTitle("+", for: .normal)
        }
        preset4ButtonIsEnabled = defaults.bool(forKey: "4")
        if(preset4ButtonIsEnabled){
            let savedname = defaults.string(forKey: "customName4")
            if(savedname != nil){
                preset4Button.setTitle(savedname, for: .normal)
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.fourthPreset.rawValue : [CameraConfig.presetName.rawValue : savedname]], replyHandler: nil, errorHandler: nil)
                    }
            }
        }else{
            preset4Button.setTitle("+", for: .normal)
        }
    }
    
    ///Setting values to all labels
    func setAllLabels(){
        apertureValueLabel.text = "f " + String(apertureValue)
        isoValueLabel.text = String(isoValue)
        shutterValueLabel.text = "1/" + String(shutterSpeedValue)
        wbValueLabel.text = String(wbValue) + "K"
        zoomValueLabel.text = String(Int(zoomValue * 1000))
        focusValueLabel.text = String(Int(focusValue * 1000))
        tintValueLabel.text = String(tintValue)
    }
    
    ///Function to show brief messages to user
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 65, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = .systemFont(ofSize: 18.0)
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.5, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
            
    func configureWatchKitSession() {
        if (WCSession.isSupported()) {
            print("Starting vessasession")
            session = WCSession.default
            session?.delegate = self
            session?.activate()
        }
    }
    
    ///Setting custom name for button
    func setNameForButton(buttonNumber: String){
        switch buttonNumber{
            case "1":
                preset1Button.setTitle(presetName, for: .normal)
            case "2":
                preset2Button.setTitle(presetName, for: .normal)
            case "3":
                preset3Button.setTitle(presetName, for: .normal)
            case "4":
                preset4Button.setTitle(presetName, for: .normal)
            default:
                presetName = ""
        }
    }
    
    // MARK: Segues
    ///Segue for closing the preset saving view
    @IBAction func UnwindPresetView(unwindSegue: UIStoryboardSegue){
        saveValuesForPreset(savingWB: wbIsSaving, savingIso: isoIsSaving, savingShutter: shutterIsSaving, savingAperture: apertureIsSaving, savingFocus: focusIsSaving, savingZoom: zoomIsSaving)
        if presetName != ""{
            let nameKey = "customName" + String(presetToSave)
            defaults.setValue(presetName, forKey: nameKey)
            setNameForButton(buttonNumber: presetToSave)
        }
        presetName = ""
        wbIsSaving = false
        tintIsSaving = false
        isoIsSaving = false
        shutterIsSaving = false
        apertureIsSaving = false
        focusIsSaving = false
        zoomIsSaving = false
        
    }
    
    ///Segue for opening the preset saving view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let vc = segue.destination as? PresetSavingViewController
        vc?.wbValue = Float(wbValue)
        vc?.isoValue = Float(isoValue)
        vc?.shutterValue = Float(shutterSpeedValue)
        vc?.apertureValue = Double(apertureValue)
        vc?.focusValue = Float(focusValue)
        vc?.zoomValue = Float(zoomValue)
        
        if segue.identifier == "preset1Segue"
        {
            vc?.presetIndex = 1
        }
        if segue.identifier == "preset2Segue"
        {
            vc?.presetIndex = 2
        }
        if segue.identifier == "preset3Segue"
        {
            vc?.presetIndex = 3
        }
        if segue.identifier == "preset4Segue"
        {
            vc?.presetIndex = 4
        }
    }
    
    // MARK: - Peripheral (connected device via bluetooth)
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else {
            return
        }
        for service in services {
            //print(service)
            peripheral.discoverCharacteristics(nil, for: service)
            
        }
    }
 
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        guard let characteristics = service.characteristics else {
            return
        }
        for characteristic in characteristics {
            //print(characteristic)
            //print(characteristic.service)
            //print(characteristic.properties)
            if(characteristic.properties.contains(.read)){
                peripheral.readValue(for: characteristic)
                print("Can Read: ", characteristic.uuid)
            }
            
            if(characteristic.properties.contains(.notify)){
                peripheral.setNotifyValue(true, for: characteristic)
                print("Can Notify: " , characteristic.uuid)
            }
            if(characteristic.uuid.uuidString == incomingCameraControlCharacteristicUUID){
                peripheral.setNotifyValue(true, for: characteristic)
                print("Incoming camera control properties: \(String(describing: characteristic.descriptors))")
                print("set notify true?")
            }
            if (characteristic.uuid.uuidString == cameraStatusCharacteristicUUID) {
                peripheral.setNotifyValue(true, for: characteristic)
                cameraStatusCharacteristic = characteristic
                ProtocolPacketLibrary.sharedLibrary.statusCharacteristic = cameraStatusCharacteristic
            }
            if(characteristic.uuid.uuidString == outgoingCameraControlCharacteristicUUID){
                print("outgoingCameraControl found.")
                print("AutoFocus")
                outgoingWriteCharacteristic = characteristic
                ProtocolPacketLibrary.sharedLibrary.outgoingCharacteristic = outgoingWriteCharacteristic
                ProtocolPacketLibrary.sharedLibrary.peripheral = myPeripheral
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Succesfully wrote to: \(characteristic)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let data = characteristic.value {
            var bytes = Array(repeating: 0 as UInt8, count: data.count/MemoryLayout<UInt8>.size)
            data.copyBytes(to: &bytes, count: data.count)
            let data8 = bytes.map { UInt8($0)}
            let convertedData = byteIntepreter.intepretCameraControlBytes(bytes: data8)
            
            print("Charasteristic value in bytes: \(data8)")
            print("Charasteristic value converted: \(String(describing: convertedData))")
            
            /// Get the message key from the converted bytes and establish business logic switch case for further use
            var messageKey : String?
            if (convertedData != nil) {
                messageKey = Array(convertedData!.keys)[0]
                
                /// Check message key to decide action
                switch messageKey {
                
                case CameraConfig.startRecord.rawValue:
                    print(CameraConfig.startRecord.rawValue)
                    print(convertedData![messageKey!] as Any )
                    DispatchQueue.global(qos: .background).async {
                        self.session?.sendMessage([CameraConfig.record.rawValue : true], replyHandler: nil, errorHandler: nil)
                    }
                case CameraConfig.stopRecord.rawValue:
                    print(CameraConfig.startRecord.rawValue)
                    print(convertedData![messageKey!] as Any )
                    DispatchQueue.global(qos: .background).async {
                        self.session?.sendMessage([CameraConfig.record.rawValue : false], replyHandler: nil, errorHandler: nil)
                    }
                case CameraConfig.autoFocus.rawValue:
                    print(CameraConfig.autoFocus.rawValue)
                    print(convertedData![messageKey!] as Any )
                case CameraConfig.ISO.rawValue:
                    print(CameraConfig.ISO.rawValue)
                    print(convertedData![messageKey!] as Any )
                    isoValue = convertedData![messageKey!] as Any as! Int32
                    DispatchQueue.global(qos: .background).async {
                        self.session?.sendMessage([messageKey! : self.isoValue], replyHandler: nil, errorHandler: nil)
                    }
                case CameraConfig.apertureFstop.rawValue:
                    print(CameraConfig.apertureFstop.rawValue)
                    print(convertedData![messageKey!] as Any)
                    let unroundedValue = convertedData![messageKey!] as Any as! Double
                    apertureValue = Double(round(10 * unroundedValue) / 10)
                    DispatchQueue.global(qos: .background).async {
                        self.session?.sendMessage([messageKey! : self.apertureValue], replyHandler: nil, errorHandler: nil)
                    }
                case CameraConfig.absoluteFocus.rawValue:
                    print(CameraConfig.absoluteFocus.rawValue)
                    print(convertedData![messageKey!] as Any )
                    let initialFocus = convertedData![messageKey!] as Any as! Double
                    focusValue = Double(round(1000*initialFocus)/1000)
                    DispatchQueue.global(qos: .background).async {
                        self.session?.sendMessage([messageKey!: self.focusValue], replyHandler: nil, errorHandler: nil)
                    }
                case CameraConfig.contZoom.rawValue:
                    print(CameraConfig.contZoom.rawValue)
                    print(convertedData![messageKey!] as Any )
                case CameraConfig.levelZoom.rawValue:
                    print(CameraConfig.contZoom.rawValue)
                    print(convertedData![messageKey!] as Any )
                case CameraConfig.shutterSpeed.rawValue:
                    print(CameraConfig.shutterSpeed.rawValue)
                    print(convertedData![messageKey!] as Any )
                    shutterSpeedValue = convertedData![messageKey!] as! Int32
                    DispatchQueue.global(qos: .background).async {
                        self.session?.sendMessage([messageKey! : self.shutterSpeedValue], replyHandler: nil, errorHandler: nil)
                    }
                case CameraConfig.autoAperture.rawValue:
                    print(CameraConfig.autoAperture.rawValue)
                    print(convertedData![messageKey!] as Any )
                case CameraConfig.pureWhiteBalance.rawValue:
                    print(convertedData![messageKey!] as Any )
                    wbValue = Int16(convertedData![messageKey!] as! Int16)
                    DispatchQueue.global(qos: .background).async {
                        self.session?.sendMessage([messageKey! : self.wbValue], replyHandler: nil, errorHandler: nil)
                    }
                default:
                    print("Could not get message value")
                }
            }
            setAllLabels()
        }
    }
    
    func subscribeToNotifications(peripheral: CBPeripheral, characteristic: CBCharacteristic){
        peripheral.setNotifyValue(true, for: characteristic)
    }
    
    // MARK: Saving functions
    ///Saving all values in UserDefaults
    func saveValuesForPreset(savingWB: Bool, savingIso: Bool, savingShutter: Bool, savingAperture: Bool, savingFocus: Bool, savingZoom: Bool){
        let savingKey = "preset" + presetToSave + "Values"
        print("Saving key: " + savingKey)
        let savedValues : [String : Any?] =
            [CameraConfig.presetName.rawValue : "PRESETNAMEHERE",
             CameraConfig.pureWhiteBalance.rawValue: wbValue,
             CameraConfig.ISO.rawValue : isoValue,
             CameraConfig.shutterSpeed.rawValue: shutterSpeedValue,
             CameraConfig.apertureFstop.rawValue: apertureValue,
             CameraConfig.absoluteFocus.rawValue : focusValue,
             CameraConfig.levelZoom.rawValue: zoomValue]
        let sendablePreset : [String : Any?] =
            [CameraConfig.presetName.rawValue : presetName]
        defaults.set(savedValues, forKey: savingKey)
        defaults.set(savingWB, forKey: "wb" + presetToSave)
        print("Saving WB key: " + "wb" + presetToSave)
        defaults.set(savingIso, forKey: "iso" + presetToSave)
        defaults.set(savingShutter, forKey: "shutter" + presetToSave)
        defaults.set(savingAperture, forKey: "aperture" + presetToSave)
        defaults.set(savingFocus, forKey: "focus" + presetToSave)
        defaults.set(savingZoom, forKey: "zoom" + presetToSave)
        defaults.set(true, forKey: presetToSave)
    
        DispatchQueue.global(qos: .background).async {
            switch savingKey {
            case PresetConfig.preset1Values.rawValue:
                self.session?.sendMessage([CameraConfig.firstPreset.rawValue: sendablePreset], replyHandler: nil, errorHandler: nil)
            case PresetConfig.preset2Values.rawValue:
                self.session?.sendMessage([CameraConfig.secondPreset.rawValue: sendablePreset], replyHandler: nil, errorHandler: nil)
            case PresetConfig.preset3Values.rawValue:
                self.session?.sendMessage([CameraConfig.thirdPreset.rawValue: sendablePreset], replyHandler: nil, errorHandler: nil)
            case PresetConfig.preset4Values.rawValue:
                self.session?.sendMessage([CameraConfig.fourthPreset.rawValue: sendablePreset], replyHandler: nil, errorHandler: nil)
            default:
                print("Could not set presets")
        }
        }
    }
    
    ///Getting all values from UserDefaults
    func getValuesFromPreset(valuesKey: String){
        let key = "preset" + valuesKey + "Values"
        let savedValues = defaults.object(forKey: key) as? [String: Any?]
        print("Opening key: " + key)
        if savedValues != nil{
            print("Opening WB key: " + "wb" + valuesKey)
            if(defaults.bool(forKey: "wb" + valuesKey)){
                wbValue = savedValues![CameraConfig.pureWhiteBalance.rawValue] as! Int16
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.pureWhiteBalance.rawValue : self.wbValue], replyHandler: nil, errorHandler: nil)
                }
            }
            if(defaults.bool(forKey: "iso" + valuesKey)){
                isoValue = savedValues![CameraConfig.ISO.rawValue] as! Int32
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.ISO.rawValue : self.isoValue], replyHandler: nil, errorHandler: nil)
                }
            }
            if(defaults.bool(forKey: "shutter" + valuesKey)){
                shutterSpeedValue = savedValues![CameraConfig.shutterSpeed.rawValue] as! Int32
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.shutterSpeed.rawValue : self.shutterSpeedValue], replyHandler: nil, errorHandler: nil)
                }
            }
            if(defaults.bool(forKey: "aperture" + valuesKey)){
                apertureValue = savedValues![CameraConfig.apertureFstop.rawValue] as! Double
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.apertureFstop.rawValue : self.apertureValue], replyHandler: nil, errorHandler: nil)
                }
            }
            if(defaults.bool(forKey: "focus" + valuesKey)){
                focusValue = savedValues![CameraConfig.absoluteFocus.rawValue] as! Double
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.absoluteFocus.rawValue : self.focusValue], replyHandler: nil, errorHandler: nil)
                }
            }
            if(defaults.bool(forKey: "zoom" + valuesKey)){
                zoomValue = savedValues![CameraConfig.levelZoom.rawValue] as! Double
                DispatchQueue.global(qos: .background).async {
                    self.session?.sendMessage([CameraConfig.levelZoom.rawValue : self.zoomValue], replyHandler: nil, errorHandler: nil)
                }
            }
            setValuesInCamera()
        } else{
            print("Error getting values in UserDefaults")
        }
    }
    
    ///Setting all values in camera
    func setValuesInCamera(){
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setManualWhiteBalance(colorTemp: Int16(self.wbValue), tint: Int16(0))
            ProtocolPacketLibrary.sharedLibrary.setISO(isoValue: self.isoValue)
            ProtocolPacketLibrary.sharedLibrary.setShutterSpeed(shutterSpeedValue: self.shutterSpeedValue)
            ProtocolPacketLibrary.sharedLibrary.setAperture(apertureValue: self.apertureValue, autoApertureOn: false)
            ProtocolPacketLibrary.sharedLibrary.setAbsoluteFocus(absoluteFocusValue: self.focusValue)
            ProtocolPacketLibrary.sharedLibrary.setZoomNormalized(zoomLevel: self.zoomValue, absoluteZoom: true)
        }
    }
    
    // MARK: Button functions
    @IBAction func recordButtonPressed(_ sender: UIButton) {
        print("Record button pressed")
        if(!recordButton.isSelected) {
            recordStatus = true
            self.session?.sendMessage([CameraConfig.record.rawValue : true], replyHandler: nil, errorHandler: nil)
        } else {
            recordStatus = false
            self.session?.sendMessage([CameraConfig.record.rawValue : false], replyHandler: nil, errorHandler: nil)
        }
    }
    
    @IBAction func autoFocusButtonPressed(_ sender: UIButton) {
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.autoFocus()
            self.session?.sendMessage([CameraConfig.absoluteFocus.rawValue: self.focusValue], replyHandler: nil, errorHandler: nil)
        }
    }
    ///Preset buttons held for some time
    @IBAction func preset1Held(_ sender: UILongPressGestureRecognizer) {
        self.performSegue(withIdentifier: "preset1Segue", sender: self)
    }
    
    @IBAction func preset2Held(_ sender: UILongPressGestureRecognizer) {
        self.performSegue(withIdentifier: "preset2Segue", sender: self)
    }
    
    @IBAction func preset3Held(_ sender: UILongPressGestureRecognizer) {
        self.performSegue(withIdentifier: "preset3Segue", sender: self)
    }
    
    @IBAction func preset4Held(_ sender: UILongPressGestureRecognizer) {
        self.performSegue(withIdentifier: "preset4Segue", sender: self)
    }
    
    ///Preset buttons tapped
    @IBAction func preset1ButtonPressed(_ sender: UIButton) {
        if(preset1ButtonIsEnabled){
            getValuesFromPreset(valuesKey: "1")
                self.showToast(message: "Opened preset 1")
        }else{
            self.showToast(message: "No saved values")
        }
    }
    @IBAction func preset2ButtonPressed(_ sender: UIButton) {
        if(preset2ButtonIsEnabled){
                getValuesFromPreset(valuesKey: "2")
                self.showToast(message: "Opened preset 2")
        }else{
            self.showToast(message: "No saved values")
        }
    }
    @IBAction func preset3ButtonPressed(_ sender: UIButton) {
        if(preset3ButtonIsEnabled){
                getValuesFromPreset(valuesKey: "3")
                self.showToast(message: "Opened preset 3")
        }else{
            self.showToast(message: "No saved values")
        }
    }
    @IBAction func preset4ButtonPressed(_ sender: UIButton) {
        if(preset4ButtonIsEnabled){
                getValuesFromPreset(valuesKey: "4")
                self.showToast(message: "Opened preset 4")
        }else{
            self.showToast(message: "No saved values")
        }
    }
    
    // MARK: Aperture sliders
    @IBAction func apertureSliderChanged(){
        apertureSliderValue = roundf(apertureSlider.value)
    }
    @objc func apertureSliderDidEndSliding(){
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setAperture(apertureValue: self.apertureValue, autoApertureOn: false)
            self.session?.sendMessage([CameraConfig.apertureFstop.rawValue : self.apertureValue], replyHandler: nil, errorHandler: nil)
        }
    }
    
    // MARK: - ISO sliders
    @IBAction func isoSliderChanged(_ sender: UISlider) {
        let sliderValue = roundf(isoSlider.value)
        isoSliderValue = sliderValue
    }
    @objc func isoSliderDidEndSliding(){
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setISO(isoValue: self.isoValue)
            self.session?.sendMessage([CameraConfig.ISO.rawValue : self.isoValue], replyHandler: nil, errorHandler: nil)
        }
    }
    
    // MARK: - Focus sliders
    @IBAction func focusSliderChanged(_ sender: UISlider) {
        focusSliderValue = roundf(focusSlider.value)
    }
    @objc func focusSliderDidEndSliding(){
        print(focusValue)
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setAbsoluteFocus(absoluteFocusValue: self.focusValue)
            self.session?.sendMessage([CameraConfig.absoluteFocus.rawValue: self.focusValue], replyHandler: nil, errorHandler: nil)
        }
    }
    
    // MARK: - Shutter sliders
    @IBAction func shutterSliderChanged(_ sender: UISlider) {
        let sliderValue = roundf(shutterSlider.value)
        shutterSliderValue = sliderValue
    }
    @objc func shutterSliderDidEndSliding(){
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setShutterSpeed(shutterSpeedValue: self.shutterSpeedValue)
            self.session?.sendMessage([CameraConfig.shutterSpeed.rawValue: self.shutterSpeedValue], replyHandler: nil, errorHandler: nil)
        }
    }
    
    // MARK: - White balance sliders
    @IBAction func wbSliderChanged(_ sender: UISlider) {
        wbValueSlider = roundf(wbSlider.value)
    }
    @objc func wbSliderDidEndSliding(){
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setManualWhiteBalance(colorTemp: Int16(self.wbValue), tint: self.tintValue)
            self.session?.sendMessage([CameraConfig.pureWhiteBalance.rawValue: self.wbValue], replyHandler: nil, errorHandler: nil)
        }
    }
    
    // MARK: - Tint sliders
    @IBAction func TintSliderChanged(_ sender: UISlider) {
        tintValueSlider = roundf(tintSlider.value)
    }
    @objc func tintSliderDidEndSliding() {
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setManualWhiteBalance(colorTemp: Int16(self.wbValue), tint: self.tintValue)
            self.session?.sendMessage([CameraConfig.tint.rawValue: self.tintValue], replyHandler: nil, errorHandler: nil)
        }
    }
    
    // MARK: - Zoom sliders
    @IBAction func ZoomSliderChanged(_ sender: UISlider) {
        zoomSliderValue = roundf(zoomSlider.value)
    }
    @objc func zoomSliderDidEndSliding(){
        DispatchQueue.global(qos:.background).async{
            ProtocolPacketLibrary.sharedLibrary.setZoomNormalized(zoomLevel: self.zoomValue, absoluteZoom: true)
            self.session?.sendMessage([CameraConfig.levelZoom.rawValue: self.zoomValue], replyHandler: nil, errorHandler: nil)
        }
    }
}

// MARK: - Extension for WCSessionDelegate that contains functions for the session between the phone and the paired watch
extension DeviceViewController : WCSessionDelegate {
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    public func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    public func sessionDidDeactivate(_ session: WCSession) {
    }
    
    public func session(_ session: WCSession, didReceiveMessageData messageData: Data) {
        print(messageData)
    }
    
    public func session(_ session: WCSession, didReceiveMessage message: [String: Any]) {
        print(message)
        let messageKey = Array(message.keys)[0]
        let queue = DispatchQueue(label: "PacketLibraryQueue", qos: .background)
        
        queue.sync {
        /// Check message key to decide action
            switch messageKey {
            
            case CameraConfig.startRecord.rawValue:
                print(CameraConfig.startRecord.rawValue)
                DispatchQueue.main.async {
                    self.recordStatus = true
                }
                
            case CameraConfig.stopRecord.rawValue:
                print(CameraConfig.startRecord.rawValue)
                ProtocolPacketLibrary.sharedLibrary.stopRecordVideo()
                DispatchQueue.main.async {
                    self.recordStatus = false
                }
                
            case CameraConfig.autoFocus.rawValue:
                print(CameraConfig.autoFocus.rawValue)
                ProtocolPacketLibrary.sharedLibrary.autoFocus()
                
            case CameraConfig.ISO.rawValue:
                print(CameraConfig.ISO.rawValue)
                DispatchQueue.main.async {
                    self.isoValue = message[CameraConfig.ISO.rawValue] as! Int32
                    ProtocolPacketLibrary.sharedLibrary.setISO(isoValue: self.isoValue)
                }
            case CameraConfig.apertureFstop.rawValue:
                print(CameraConfig.apertureFstop.rawValue)
                DispatchQueue.main.async {
                    self.apertureValue = message[CameraConfig.apertureFstop.rawValue] as! Double
                    print("Incoming aperture: \(self.apertureValue)")
                    ProtocolPacketLibrary.sharedLibrary.setAperture(apertureValue: self.apertureValue, autoApertureOn: false)
                }
                
            case CameraConfig.absoluteFocus.rawValue:
                print(CameraConfig.absoluteFocus.rawValue)
                DispatchQueue.main.async {
                    self.focusValue = (message[CameraConfig.absoluteFocus.rawValue] as? Double)!
                    ProtocolPacketLibrary.sharedLibrary.setAbsoluteFocus(absoluteFocusValue: self.focusValue)
                }
                
            case CameraConfig.contZoom.rawValue:
                print(CameraConfig.contZoom.rawValue)
                ProtocolPacketLibrary.sharedLibrary.setContinuousZoom(zoomRate: message[messageKey] as? Double)
            
            case CameraConfig.levelZoom.rawValue:
                print(messageKey)
                ProtocolPacketLibrary.sharedLibrary.setZoomNormalized(zoomLevel: message[messageKey] as? Double, absoluteZoom: true)
                DispatchQueue.main.async {
                    self.zoomValue = (message[CameraConfig.levelZoom.rawValue] as? Double)!
                }
                
            case CameraConfig.shutterSpeed.rawValue:
                print(CameraConfig.shutterSpeed.rawValue)
                DispatchQueue.main.async {
                    self.shutterSpeedValue = (message[CameraConfig.shutterSpeed.rawValue] as? Int32)!
                    ProtocolPacketLibrary.sharedLibrary.setShutterSpeed(shutterSpeedValue: self.shutterSpeedValue)
                }
                
            case CameraConfig.autoAperture.rawValue:
                print(CameraConfig.autoAperture.rawValue)
                ProtocolPacketLibrary.sharedLibrary.setAperture(apertureValue: 0, autoApertureOn: true)
                
            case CameraConfig.pureWhiteBalance.rawValue:
                DispatchQueue.main.async {
                    self.wbValue = message[CameraConfig.pureWhiteBalance.rawValue] as! Int16
                    ProtocolPacketLibrary.sharedLibrary.setManualWhiteBalance(colorTemp: self.wbValue, tint: self.tintValue)
                }
            case CameraConfig.tint.rawValue:
                DispatchQueue.main.async {
                    self.tintValue = message[CameraConfig.tint.rawValue] as! Int16
                    ProtocolPacketLibrary.sharedLibrary.setManualWhiteBalance(colorTemp: self.wbValue, tint: self.tintValue)
                }
            case CameraConfig.firstPreset.rawValue:
                print(CameraConfig.firstPreset.rawValue)
                Dispatch.DispatchQueue.main.async {
                    self.getValuesFromPreset(valuesKey: "1")
                }
            case CameraConfig.secondPreset.rawValue:
                print(CameraConfig.secondPreset.rawValue)
                Dispatch.DispatchQueue.main.async {
                    self.getValuesFromPreset(valuesKey: "2")
                }
            case CameraConfig.thirdPreset.rawValue:
                print(CameraConfig.thirdPreset.rawValue)
                Dispatch.DispatchQueue.main.async {
                    self.getValuesFromPreset(valuesKey: "3")
                }
            case CameraConfig.fourthPreset.rawValue:
                print(CameraConfig.fourthPreset.rawValue)
                Dispatch.DispatchQueue.main.async {
                    self.getValuesFromPreset(valuesKey: "4")
                }
            default:
                print("Could not get message value")
            }
        }
    }
}
