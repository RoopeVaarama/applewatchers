//
//  PresetSavingViewController.swift
//  AppleWatchers
//
//  Created by Lauri Nissinen on 4.12.2020.
//

import UIKit

class PresetSavingViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    @IBOutlet weak var wbButton: UIButton!
    @IBOutlet weak var tintButton: UIButton!
    @IBOutlet weak var isoButton: UIButton!
    @IBOutlet weak var shutterButton: UIButton!
    @IBOutlet weak var apertureButton: UIButton!
    @IBOutlet weak var focusButton: UIButton!
    @IBOutlet weak var zoomButton: UIButton!
    @IBOutlet weak var wbLabel: UILabel!
    @IBOutlet weak var tintLabel: UILabel!
    @IBOutlet weak var isoLabel: UILabel!
    @IBOutlet weak var shutterLabel: UILabel!
    @IBOutlet weak var apertureLabel: UILabel!
    @IBOutlet weak var namingLabel: UITextField!
    @IBOutlet weak var focusLabel: UILabel!
    @IBOutlet weak var zoomLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    var presetIndex = 0
    var wbValue = Float(0)
    var tintValue = Float(0)
    var isoValue = Float(0)
    var shutterValue = Float(0)
    var apertureValue = Double(0)
    var apertureText = ""
    var focusValue = Float(0)
    var zoomValue = Float(0)
    var presetName = ""
    
    let apertureValueConverter:  ApertureValueConverter = ApertureValueConverter()
    
    // MARK: Basic functions
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(PresetSavingViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        apertureText = apertureValueConverter.convertApertureValueToFNumber(apertureValue: apertureValue)
        namingLabel.delegate = self

        wbLabel.text = String(Int(wbValue)) + "K"
        tintLabel.text = String(tintValue)
        isoLabel.text = String(Int(isoValue))
        shutterLabel.text = "1/" + String(Int(shutterValue))
        apertureLabel.text = String(apertureText)
        focusLabel.text = String(Int(focusValue * 1000))
        zoomLabel.text = String(Int(zoomValue * 1000))
    }
    
    ///Hide keyboard
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    ///Segue for leaving the preset saving view
    override func prepare(for unwindSegue: UIStoryboardSegue, sender: Any?)
    {
        let vc = unwindSegue.destination as? DeviceViewController
        
        vc?.wbIsSaving = wbButton.isSelected
        vc?.tintIsSaving = tintButton.isSelected
        vc?.isoIsSaving = isoButton.isSelected
        vc?.shutterIsSaving = shutterButton.isSelected
        vc?.apertureIsSaving = apertureButton.isSelected
        vc?.focusIsSaving = focusButton.isSelected
        vc?.zoomIsSaving = zoomButton.isSelected
        
        if namingLabel.text != ""{
            vc?.presetName = namingLabel.text!
        }
        
        if presetIndex == 1
        {
            vc?.preset1Button.setTitle("Preset 1", for: .normal)
            vc?.preset1ButtonIsEnabled = true
            vc?.presetToSave = "1"
        }
        if presetIndex == 2{
        
            vc?.preset2Button.setTitle("Preset 2", for: .normal)
            vc?.preset2ButtonIsEnabled = true
            vc?.presetToSave = "2"
        }
        if presetIndex == 3
        {
            vc?.preset3Button.setTitle("Preset 3", for: .normal)
            vc?.preset3ButtonIsEnabled = true
            vc?.presetToSave = "3"
        }
        if presetIndex == 4
        {
            vc?.preset4Button.setTitle("Preset 4", for: .normal)
            vc?.preset4ButtonIsEnabled = true
            vc?.presetToSave = "4"
        }
    }
    
    // MARK: Button functions
    @IBAction func wbButtonPressed(_ sender: UIButton) {
        if(wbButton.isSelected){
            wbButton.isSelected = false
        }else{
            wbButton.isSelected = true
        }
    }
    @IBAction func tintButtonPressed(_ sender: UIButton) {
        if(tintButton.isSelected){
            tintButton.isSelected = false
        }else{
            tintButton.isSelected = true
        }
    }
    
    @IBAction func isoButtonPressed(_ sender: UIButton) {
        if(isoButton.isSelected){
            isoButton.isSelected = false
        }else{
            isoButton.isSelected = true
        }
    }
    
    @IBAction func shutterButtonPressed(_ sender: UIButton) {
        if(shutterButton.isSelected){
            shutterButton.isSelected = false
        }else{
            shutterButton.isSelected = true
        }
    }
    
    @IBAction func apertureButtonPressed(_ sender: UIButton) {
        if(apertureButton.isSelected){
            apertureButton.isSelected = false
        }else{
            apertureButton.isSelected = true
        }
    }
    
    @IBAction func focusButtonPressed(_ sender: UIButton) {
        if(focusButton.isSelected){
            focusButton.isSelected = false
        }else{
            focusButton.isSelected = true
        }
    }
    @IBAction func zoomButtonPressed(_ sender: UIButton) {
        if(zoomButton.isSelected){
            zoomButton.isSelected = false
        }else{
            zoomButton.isSelected = true
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
    }
    
    ///Restricting preset naming in 8 characters
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let current = textField.text ?? ""
        guard let stringRange = Range(range, in: current) else { return false }
        let updated = current.replacingCharacters(in: stringRange, with: string)
        return updated.count <= 8
    }
}
