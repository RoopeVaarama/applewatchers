//
//  PacketDecoder.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 4.11.2020.
//

import Foundation

// MARK: - Contains functions that are utilized in converting values in various formats to bytes

class ValueToByteConverter {
    
    /// Converts an unlimited size hexadecimal string to a UInt8 byte array as long as
    /// hexadecimal size is even.
    ///
    /// - Parameter hexString: hexadecimal in string format
    /// - Returns: UInt8 byte array if hex size is even, nil if it is odd
    func decodeHex(hexString : String) -> [UInt8]? {
        let length = hexString.count
        if length & 1 != 0 {
            return nil
        }
        var byteArray = [UInt8]()
        byteArray.reserveCapacity(length/2)
        var index = hexString.startIndex
        for _ in 0..<length/2 {
            let nextIndex = hexString.index(index, offsetBy: 2)
            if let b = UInt8(hexString[index..<nextIndex], radix: 16) {
                byteArray.append(b)
            } else {
                return nil
            }
            index = nextIndex
        }
        return byteArray
    }
    
    /// Converts an integer that conforms to SignedInteger (in this case Int32 or Int16) to a UInt8 byte array or nil on failure.
    ///
    /// - Warning: Contains a check for whether the parameter is Int32 or Int16. Returns nil if conditions are not met.
    /// - Parameter int: Generic type int that has to conform to SignedInteger. Pass in either Int32 or Int16
    /// - Returns: UInt8 byte array or nil
    func decimalToBytes<T: SignedInteger>(int: T) -> [UInt8]? {
        let length = 8
        var byteArray = [UInt8]()
        byteArray.reserveCapacity(length/2)
        var st : String
        if (int is Int16) {
            st = String(format: "%02X", int as! CVarArg)
        } else if (int is Int32) {
            st = String(format: "%02X", int as! CVarArg)
        } else {
            return nil
        }
        print(st)
        var stringArray = Array(st)
        if (stringArray.count % 2 != 0) {
            stringArray.insert("0", at: 0)
            st.insert("0", at: st.startIndex)
        }
        print(stringArray)
        var index = st.startIndex
        for i in 0..<length/2 {
            print(i)
            if (i < st.count / 2) {
                let nextIndex = st.index(index, offsetBy: 2)
                let hex = st[index..<nextIndex]
                print(String(hex))
                let bytes = decodeHex(hexString: String(hex))!
                byteArray.insert(bytes[0], at: 0)
                index = nextIndex
            } else if (i < length/2) {
                let uint8 : UInt8 = 00
                byteArray.append(uint8)
            }
        }
        return byteArray
    }
}
