//
//  ShutterSpeedValueConverter.swift
//  LVZwatch
//
//  Created by Topias Peiponen on 1.12.2020.
//

import Foundation

// MARK: Contains functions and conversions for shutter speed and it's respective slider values
class ShutterSpeedValueConverter {
    // MARK: - Set minimum and maximum parameter values
    let maxSliderValue = 7
    let minSliderValue = 0
    let maxShutterSpeed = 2000
    let minShutterSpeed = 60
    
    /// Converts a slider value to it's respective shutter speed
    ///
    /// - Parameter sliderValue: Float slider value that can be got from every slider on the platform.
    /// - Returns: Shutter speed value
    func convertSliderValueToShutterSpeed(sliderValue : Float) -> Int32? {
        var shutterSpeedValue : Int32?
        switch sliderValue {
            case 0:
                shutterSpeedValue = Int32(2000)
            case 1:
                shutterSpeedValue = Int32(1000)
            case 2:
                shutterSpeedValue = Int32(500)
            case 3:
                shutterSpeedValue = Int32(250)
            case 4:
                shutterSpeedValue = Int32(200)
            case 5:
                shutterSpeedValue = Int32(125)
            case 6:
                shutterSpeedValue = Int32(100)
            case 7:
                shutterSpeedValue = Int32(60)
            default:
                shutterSpeedValue = nil
        }
        return shutterSpeedValue
    }
    
    /// Converts a slider value to it's respective shutter speed
    ///
    /// - Parameter shutterSpeed: Shutter speed value in Int32
    /// - Returns: Slider value in float
    func convertShutterSpeedToSliderValue(shutterSpeed : Int32) -> Float? {
        var sliderValue : Float?
        switch shutterSpeed {
        case 2000:
            sliderValue = 0
        case 1000:
            sliderValue = 1
        case 500:
            sliderValue = 2
        case 250:
            sliderValue = 3
        case 200:
            sliderValue = 4
        case 125:
            sliderValue = 5
        case 100:
            sliderValue = 6
        case 60:
            sliderValue = 7
        default:
            sliderValue = nil
        }
        return sliderValue
    }
}
