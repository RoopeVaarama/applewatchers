import Foundation

func convertApertureValueToFNumber(apertureVale : Double) -> String {
    let fNumber = sqrt(pow(2, apertureVale))
    let roundedFNumber = Double(round(10*fNumber) / 10)
    return "f\(roundedFNumber)"
}

let unroundedValue = 2.1243543523468746
let apertureValue = Double(round(10 * unroundedValue) / 10)
let futureApertureValue = Double(154) / 10
let initialFocus = 0.23452345
let focusValue = Double(round(1000*initialFocus)/1000)
