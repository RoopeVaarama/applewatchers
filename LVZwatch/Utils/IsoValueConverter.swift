//
//  ISOValueConverter.swift
//  LVZwatch
//
//  Created by Topias Peiponen on 1.12.2020.
//

import Foundation

// MARK: - Contains functions and conversions for ISO values and its respective slider values
class ISOValueConverter {
    
    /// Converts ISO to slider value
    ///
    /// - Parameter isoValue: ISO value in Int32. ISO values vary between camera peripheral but in general 100 - 25600
    /// - Returns: Slider value in float
    func convertISOToSliderValue(isoValue : Int32) -> Float? {
        var sliderValue : Float?
        switch isoValue{
            case 100:
                sliderValue = 0
            case 200:
                sliderValue = 1
            case 400:
                sliderValue = 2
            case 800:
                sliderValue = 3
            case 1600:
                sliderValue = 4
            case 3200:
                sliderValue = 5
            case 6400:
                sliderValue = 6
            default:
                sliderValue = nil
        }
        return sliderValue
    }
    
    /// Converts slider value to it's respective ISO value
    ///
    /// - Parameter sliderValue: Slider value in float. This value can be gotten from any xCode slider
    /// - Returns: ISO value in Int32
    func convertSliderValueToISO(sliderValue : Float) -> Int32? {
        var isoValue : Int32?
        switch sliderValue{
            case 0:
                isoValue = 100
            case 1:
                isoValue = 200
            case 2:
                isoValue = 400
            case 3:
                isoValue = 800
            case 4:
                isoValue = 1600
            case 5:
                isoValue = 3200
            case 6:
                isoValue = 6400
            default:
                isoValue = nil
        }
        return isoValue
    }
}
