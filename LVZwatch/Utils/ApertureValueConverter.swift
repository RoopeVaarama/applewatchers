//
//  ApertureValueConverter.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 27.11.2020.
//

import Foundation

// MARK: - Contains functions for handling aperture values and conversions
class ApertureValueConverter {
    /// Converts aperture value to it's f-number format
    ///
    /// - Parameter apertureValue: Aperture value in Double format in range  -1.0 - 16.0
    /// - Returns: F-number in string
    func convertApertureValueToFNumber(apertureValue : Double) -> String {
        let fNumber = sqrt(pow(2, apertureValue))
        let roundedFNumber = Double(round(10*fNumber) / 10)
        return "f \(roundedFNumber)"
    }
    
}
