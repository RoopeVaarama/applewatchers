//
//  ProtocolPackets.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 4.11.2020.
//

import Foundation
import CoreBluetooth

// MARK: - Library of commands to control camera

class ProtocolPacketLibrary {
    /// Create a singleton for shared access to the same peripheral and charasteristic
    static let sharedLibrary = ProtocolPacketLibrary()
    
    /// Create instance of ValueToByteConverter at top-level to aid in packet encoding
    let valueToByteConverter = ValueToByteConverter()
    
    // MARK: - Device information and camera control charasteristics need to be saved to top-level variables when first accessing the singleton
    private var _peripheral : CBPeripheral?
    private var _outgoingCharacteristic : CBCharacteristic?
    private var _statusCharacteristic : CBCharacteristic?

    var peripheral : CBPeripheral? {
        get {
            return _peripheral
        }
        set {
            _peripheral = newValue
        }
    }
    var outgoingCharacteristic : CBCharacteristic? {
        get {
            return _outgoingCharacteristic
        }
        set {
            _outgoingCharacteristic = newValue
        }
    }
    var statusCharacteristic : CBCharacteristic? {
        get {
            return _statusCharacteristic
        }
        set {
            _statusCharacteristic = newValue
        }
    }
    
    private init() {
    }
    
    // MARK: - These functions are void functions and their packets are always static requiring NO mutable values.
    
    func autoFocus() {
        if (_peripheral != nil && _outgoingCharacteristic != nil) {
            print("Starting auto focus...")
            let autoFocusBytes : [UInt8] = [1, 4, 0, 0, 0, 1, 0, 0]
            let data = Data(autoFocusBytes)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not auto focus.")
        }
    }
    
    func startRecordVideo() {
        if (_peripheral != nil && _outgoingCharacteristic != nil) {
            print("Starting to record...")
            let startRecordVideoBytes : [UInt8] = [1, 9, 0, 0, 10, 1, 1, 0, 2, 0, 1<<1, 1, 1, 0, 0, 0]
            let data = Data(startRecordVideoBytes)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not start recording.")
        }
    }
    
    func stopRecordVideo() {
        if (_peripheral != nil && _outgoingCharacteristic != nil) {
            print("Stopping recording...")
            let stopRecordVideoBytes : [UInt8] = [1, 9, 0, 0, 10, 1, 1, 0, 0, 0, 1<<1, 1, 1, 0, 0, 0]
            let data = Data(stopRecordVideoBytes)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not stop recording.")
        }
    }
    
    // MARK: - These functions require values that need to be converted to bytes in order to form a sendable packet.
    
    /// Sets ISO level on the peripheral
    ///
    /// ISO value can be turned up to Int32.max but common iso levels are 100 - 6400
    /// - Parameter isoValue: Int32 value between 0 and 2147483647 (Int32.max)
    func setISO(isoValue : Int32) {
        let isoValueByteArray : [UInt8]? = valueToByteConverter.decimalToBytes(int : isoValue)
        
        /// ISO value was succesfully converted and byte array is not nil
        if (_peripheral != nil && _outgoingCharacteristic != nil && isoValueByteArray != nil) {
            print("Changing ISO...")
            
            var bytesISO : [UInt8] = [1, 8, 0, 0, 1, 14, 3, 0, 0x10, 0x27, 0x00, 0x00]
            bytesISO[8] = isoValueByteArray![0]
            bytesISO[9] = isoValueByteArray![1]
            bytesISO[10] = isoValueByteArray![2]
            bytesISO[11] = isoValueByteArray![3]
            print(bytesISO)
            
            let data = Data(bytesISO)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not change ISO")
        }
    }
    
    /// Sets aperture level on the peripheral
    ///
    /// - Parameter apertureValue: Double type value between -1.0 and 16.0 as per camera documentation
    /// - Parameter autoApertureOn: Boolean indicating if auto aperture should be used or not
    func setAperture(apertureValue : Double?, autoApertureOn : Bool) {
        var apertureValueBytes : [UInt8]? = nil
        /// If autoApertureOn is false and value is not nil, use required equasions to get the
        /// proper value to be converted to bytes.
        ///
        /// 1. fixedPoint = fNumber * (2^11)
        if (!autoApertureOn && apertureValue != nil) {
            let apValueToFixedPoint = Int16(apertureValue! * (pow(2,11)))
            apertureValueBytes = valueToByteConverter.decimalToBytes(int: apValueToFixedPoint)
        }
        
        /// Auto-aperture on
        if (_peripheral != nil && _outgoingCharacteristic != nil && autoApertureOn) {
            print("Triggering auto-aperture...")
            
            let bytesAperture : [UInt8] = [1, 4, 0, 0, 0, 2, 0, 0]
            let data = Data(bytesAperture)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
            
        }
        /// Auto-auperture off, use custom aperture value
        else if(_peripheral != nil && _outgoingCharacteristic != nil && !autoApertureOn && apertureValueBytes != nil) {
            print("Changing aperture...")
            
            var bytesAperture : [UInt8] = [1, 8, 0, 0, 0, 2, 128, 0, 0x0, 0x0, 0x0, 0x0]
            bytesAperture[8] = apertureValueBytes![0]
            bytesAperture[9] = apertureValueBytes![1]
            bytesAperture[10] = apertureValueBytes![2]
            bytesAperture[11] = apertureValueBytes![3]
            
            let data = Data(bytesAperture)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        }
        else {
            print("Could not change aperture")
        }
    }
    
    /// Sets absolute focus on the peripheral
    ///
    /// - Parameter absoluteFocusValue: Double type value between 0.0 and 1.0. Min 0.0 = near, max 1.0 = far
    func setAbsoluteFocus(absoluteFocusValue : Double?) {
        var absoluteFocusBytes : [UInt8]? = nil
        
        if (absoluteFocusValue != nil) {
            let focusToFixedPoint = Int16(absoluteFocusValue! * (pow(2, 11)))
            absoluteFocusBytes = valueToByteConverter.decimalToBytes(int: focusToFixedPoint)
        }
        
        if (_peripheral != nil && _outgoingCharacteristic != nil && absoluteFocusBytes != nil) {
            print("Changing absolute focus...")
            
            var bytesAbsFocus : [UInt8] = [1, 8, 0, 0, 0, 0, 128, 0, 0x0, 0x0, 0x0, 0x0]
            bytesAbsFocus[8] = absoluteFocusBytes![0]
            bytesAbsFocus[9] = absoluteFocusBytes![1]
            bytesAbsFocus[10] = absoluteFocusBytes![2]
            bytesAbsFocus[11] = absoluteFocusBytes![3]
            
            let data = Data(bytesAbsFocus)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not change absolute focus")
        }
    }
    
    /// Sets continuous zoom on the peripheral
    ///
    /// Includes zoom-in and zoom-out depending on if the value is negative or positive.
    /// - Parameter zoomRate: Double type value between - 1.0 and 1.0. Negative value zooms out, 0.0 stops zooming, positive value zooms in.
    func setContinuousZoom(zoomRate : Double?) {
        var zoomRateBytes : [UInt8]? = nil
        
        if (zoomRate != nil) {
            let zoomRateToFixedPoint = Int16(zoomRate! * (pow(2, 11)))
            zoomRateBytes = valueToByteConverter.decimalToBytes(int: zoomRateToFixedPoint)
        }
        
        if (_peripheral != nil && _outgoingCharacteristic != nil && zoomRateBytes != nil) {
            print("Changing zoom rate...")
            
            var bytesContinuousZoom : [UInt8] = [1, 6, 0, 0, 0, 9, 128, 0, 0, 0, 0, 0]
            bytesContinuousZoom[8] = zoomRateBytes![0]
            bytesContinuousZoom[9] = zoomRateBytes![1]
            
            let data = Data(bytesContinuousZoom)
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not change zoom rate focus")
        }
    }
    
    /// Sets shutter speed on the peripheral
    ///
    /// - Parameter shutterSpeedValue: Type double shutter speed value as a fraction of 1. So 50 for 1/50th of a second. Min 0, max 5000
    ///
    func setShutterSpeed(shutterSpeedValue : Int32?) {
        var shutterSpeedValueBytes : [UInt8]? = nil
        
        if (shutterSpeedValue != nil) {
            shutterSpeedValueBytes = valueToByteConverter.decimalToBytes(int: shutterSpeedValue!)
        }
        
        if (_peripheral != nil && _outgoingCharacteristic != nil && shutterSpeedValueBytes != nil) {
            print("Changing shutter speed")
            
            var bytesShutterSpeed : [UInt8] = [1, 8, 0, 0, 1, 12, 3, 0, 0, 0, 0, 0]
            bytesShutterSpeed[8] = shutterSpeedValueBytes![0]
            bytesShutterSpeed[9] = shutterSpeedValueBytes![1]
            bytesShutterSpeed[10] = shutterSpeedValueBytes![2]
            bytesShutterSpeed[11] = shutterSpeedValueBytes![3]
            let data = Data(bytesShutterSpeed)
            
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not change shutter speed")
        }
    }
    
    /// Sets shutter angle on the peripheral
    ///
    /// - Parameter shutterAngleValue: Type double shutter angle value in degrees, multiplied by 100. Minimum 100, max 36000.
    func setShutterAngle(shutterAngleValue : Int32?) {
        var shutterAngleValueBytes : [UInt8]? = nil
        
        if (shutterAngleValue != nil) {
            shutterAngleValueBytes = valueToByteConverter.decimalToBytes(int: shutterAngleValue!)
        }
        
        if (_peripheral != nil && _outgoingCharacteristic != nil && shutterAngleValueBytes != nil) {
            print("Changing shutter angle")
            
            var bytesShutterAngle : [UInt8] = [1, 8, 0, 0, 1, 12, 3, 0, 0, 0, 0, 0]
            bytesShutterAngle[8] = shutterAngleValueBytes![0]
            bytesShutterAngle[9] = shutterAngleValueBytes![1]
            bytesShutterAngle[10] = shutterAngleValueBytes![2]
            bytesShutterAngle[11] = shutterAngleValueBytes![3]
            let data = Data(bytesShutterAngle)
            
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not change shutter angle")
        }
    }
    
    /// Sets the white balance (color temperature and tint) on the peripheral
    ///
    /// - Parameter colorTemp: Color temperature in K (kelvin). Minimum is 2500, maximum is 10000.
    /// - Parameter tint: Tint value. Minimum is -50, maximum is 50
    func setManualWhiteBalance(colorTemp : Int16, tint : Int16) {
        var colorTempBytes : [UInt8]? = nil
        var tintBytes : [UInt8]? = nil
        
        colorTempBytes = valueToByteConverter.decimalToBytes(int: colorTemp)
        tintBytes = valueToByteConverter.decimalToBytes(int: tint)
        
        if (_peripheral != nil && _outgoingCharacteristic != nil && colorTempBytes != nil && tintBytes != nil) {
            print("Changing white balance")
            
            var bytesWhiteBalance : [UInt8] = [1, 8, 0, 0, 1, 2, 2, 0, 0, 0, 0, 0]
            bytesWhiteBalance[8] = colorTempBytes![0]
            bytesWhiteBalance[9] = colorTempBytes![1]
            bytesWhiteBalance[10] = tintBytes![0]
            bytesWhiteBalance[11] = tintBytes![1]
            let data = Data(bytesWhiteBalance)
            
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not change white balance")
        }
    }
    
    /// Sets the normalized zoom level
    ///
    /// - Parameter zoomLevel: Focal length/zoom level to be set. Value range is 0.0 - 1.0
    /// - Parameter absoluteZoom: Determines if absolute zoom should be used or not. True if using absolute zoom, false if using relative zoom
    func setZoomNormalized(zoomLevel : Double?, absoluteZoom : Bool) {
        var zoomBytes : [UInt8]? = nil
        
        if (zoomLevel != nil) {
            let zoomLevelToFixedPoint = Int16(zoomLevel! * (pow(2, 11)))
            zoomBytes = valueToByteConverter.decimalToBytes(int: zoomLevelToFixedPoint)
        }
        
        if (_peripheral != nil && _outgoingCharacteristic != nil && zoomBytes != nil) {
            print("Changing normalized zoom level")
            var sendableBytesZoom : [UInt8]
            
            /// Using relative zoom
            if (!absoluteZoom) {
                sendableBytesZoom = [1, 8, 0, 0, 0, 8, 128, 1, 0x00, 0x00, 0x00, 0x00]
            }
            /// Using absolute zoom
            else {
                sendableBytesZoom = [1, 8, 0, 0, 0, 8, 128, 0, 0x00, 0x00, 0x00, 0x00]
            }
            sendableBytesZoom[8] = zoomBytes![0]
            sendableBytesZoom[9] = zoomBytes![1]
            sendableBytesZoom[10] = zoomBytes![2]
            sendableBytesZoom[11] = zoomBytes![3]
            let data = Data(sendableBytesZoom)
            
            _peripheral?.writeValue(data, for: _outgoingCharacteristic!, type: .withResponse)
        } else {
            print("Could not change normalized zoom level")
        }
    }
    
    // MARK: - These functions are for sending packets to the camera status charasteristic
    
    func checkPayload() {
        print("Payload triggered \(_peripheral) \(_statusCharacteristic)")
        if (_peripheral != nil && _statusCharacteristic != nil) {
            print("Status char: \(_statusCharacteristic)")
            let byte : [UInt8] = [0x10]
            let data = Data(byte)
            
            _peripheral?.writeValue(data, for: _statusCharacteristic!, type: .withResponse)
        }
    }
    
}
