//
//  ByteIntepreter.swift
//  AppleWatchers
//
//  Created by Topias Peiponen on 17.11.2020.
//

import Foundation

/// Inteprets bytes in a packet's byte array to a readable format
class ByteIntepreter {
    
    /// Inteprets the entire byte array
    ///
    /// Main function for the intepretation that calls all the other functions in the class.
    /// - Parameter bytes: UInt8 byte array. Byte array is checked for validity and if it is in the correct format.
    /// - Returns: [String : Any?]? variable that contains the packets command type in key and the possible value for the command in value. Example: ["ISO", 400]. Value can nil, indicating that the command requires no value.
    func intepretCameraControlBytes(bytes : [UInt8]?) -> [String : Any]? {
        if (bytes!.count >= 8 && isDestDeviceValid(firstByte: bytes![0]) && isCommandIDValid(fifthByte: bytes![4])) {
            
            let commandBytes : [UInt8] = Array(arrayLiteral: bytes![4], bytes![5])
            guard let commandType = intepretCommandID(commandBytes: commandBytes) else {
                return nil
            }
            
            guard let commandValueType = intepretCommandValueType(seventhByte: bytes![6]) else {
                return nil
            }
            
            /*guard let commandOperationType = intepretOperationType(eigthByte: bytes![7]) else {
                return nil
            }*/
            
            /// Check if there are bytes left. If there are then intepret the remaining bytes as a value. If not then check if
            /// operation type was bool (void) indicating that it doesn't need value
            let remainingByteCount = bytes!.count - 8
            if (remainingByteCount > 0 && remainingByteCount % 4 == 0) {
                /// Make a new array with the remaining bytes and reverse it from the
                let remainingByteArrayUnreversed = bytes![8..<bytes!.count]
                let remainingByteArray = Array(remainingByteArrayUnreversed.reversed())
                
                /// Convert byte array to hex string for easy conversion
                let byteString = remainingByteArray.map {
                    String(format: "%02x", $0)
                }
                let byteHexString = byteString.joined()
                
                /// Iterate through command types that have more than one value in their value bytes
                switch commandType {
                
                case CameraConfig.manualWhiteBalance:
                    let byteHexStringToArray = Array(byteHexString)
                    
                    /// Get tint value from remaining bytes and convert to Int16
                    let tintValueArray = Array(byteHexStringToArray[0...3])
                    let tintHex = String(tintValueArray)
                    guard let valueTint = Int16(tintHex, radix: 16) else { return nil }
                    
                    /// Get WB value from remaining bytes and convert to Int16
                    let wbValueArray = Array(byteHexStringToArray[4...7])
                    let wbHex = String(wbValueArray)
                    guard let valueWB = Int16(wbHex, radix: 16) else { return nil }
                    
                    let returnDictionary = [CameraConfig.pureWhiteBalance.rawValue : valueWB, CameraConfig.tint.rawValue : valueTint]
                    return returnDictionary
                default:
                    print("Moving onto command value type")
                }
                
                /// Iterate through value types to convert value with proper conversion method
                switch commandValueType {
                
                case ConfigValueType.bool:
                    if(remainingByteArray[0] == 1) {
                        return [commandType.rawValue : true]
                    } else {
                        return [commandType.rawValue : false]
                    }
                case ConfigValueType.signedByte:
                    guard let value = Int8(byteHexString, radix: 16) else { return nil }
                    return [commandType.rawValue : value]
                case ConfigValueType.signed16Bit:
                    guard let value = Int16(byteHexString, radix: 16) else { return nil }
                    return [commandType.rawValue : value]
                case ConfigValueType.signed32Bit:
                    guard let value = Int32(byteHexString, radix: 16) else { return nil }
                    return [commandType.rawValue : value]
                case ConfigValueType.signedFixedPoint:
                    guard let value = Int16(byteHexString, radix: 16) else { return nil }
                    let valueFromFixedPoint = Double(value) / (pow(2, 11))
                    return [commandType.rawValue : valueFromFixedPoint]
                default:
                    return nil
                }
                
            } else if (commandValueType == ConfigValueType.bool) {
                return [commandType.rawValue : true]
            } else if (commandType == CameraConfig.record) {
                /// Make a new array with the remaining bytes
                let remainingBytes = bytes![8..<bytes!.count]
                let remainingByteArray = Array(remainingBytes)
                
                switch (remainingByteArray[0]) {
                case 0:
                    return [CameraConfig.stopRecord.rawValue : true]
                case 2:
                    return [CameraConfig.startRecord.rawValue : true]
                default:
                    return nil
                }
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    // MARK: - Functions that check if the packet should be read
    
    /// Checks if device destination is valid
    ///
    /// - Parameter firstByte: UInt8 byte that can be 0-255. This should be 255 when reading notifications from the peripheral
    /// - Returns: Bool that is true if firstByte is 255 and false otherwise.
    private func isDestDeviceValid(firstByte : UInt8) -> Bool {
        return firstByte == 255
    }
    
    /// Checks if command ID byte(s) is valid
    ///
    /// - Parameter fifthByte: UInt8 byte that can be 0-255. This is the first command byte in the entire byte array, but it decides if the entire packet is valid or not. Valid values are 0-8 and 10-11. Values 9 and 12 or above are invalid.
    /// - Returns: Bool that is true if fifthByte is in range of 0-8 or 10-11. False otherwise.
    private func isCommandIDValid(fifthByte : UInt8) -> Bool {
        return (fifthByte != 9 && fifthByte < 12)
    }
    
    // MARK: - Functions that intepret certain bytes to a readable format
    
    /// Inteprets the command ID bytes
    ///
    /// - Parameter commandBytes: UInt8 byte array with count 2 that contains the 5th and the 6th byte in the entire byte array that make up the command type.
    /// - Returns: CameraConfig enumerator value that will be the final command type of the byte array (packet). Can be nil if commandBytes is not defined.
    private func intepretCommandID(commandBytes : [UInt8]) -> CameraConfig? {
        print("Intepreting command ID: \(commandBytes)")
        switch commandBytes {
        case [0, 0]:
            return CameraConfig.absoluteFocus
        case [0, 1]:
            return CameraConfig.autoFocus
        case [0, 2]:
            return CameraConfig.apertureFstop
        case [0, 3]:
            return CameraConfig.apertureNormalized
        case [0, 5]:
            return CameraConfig.autoAperture
        case [0, 8]:
            return CameraConfig.levelZoom
        case [0, 9]:
            return CameraConfig.contZoom
        case [1, 2]:
            return CameraConfig.manualWhiteBalance
        case [1, 12]:
            return CameraConfig.shutterSpeed
        case [1, 14]:
            return CameraConfig.ISO
        case [10, 1]:
            return CameraConfig.record
        default:
            print("Could not intepret command ID")
            return nil
        }
    }
    
    /// Inteprets command's value types
    ///
    /// - Parameter seventhByte: UInt8 value that is the 7th byte in the byte array representing data type represented in the data/value bytes
    /// - Returns: ConfigValueType? enumerator value representing the data type or nil if the byte was invalid.
    private func intepretCommandValueType(seventhByte : UInt8) -> ConfigValueType? {
        switch seventhByte {
        case 0:
            return ConfigValueType.bool
        case 1:
            return ConfigValueType.signedByte
        case 2:
            return ConfigValueType.signed16Bit
        case 3:
            return ConfigValueType.signed32Bit
        case 4:
            return ConfigValueType.signed64Bit
        case 5:
            return ConfigValueType.utf8String
        case 128:
            return ConfigValueType.signedFixedPoint
        default:
            print("Could not intepret command value type.")
            return nil
        }
    }
    
    /// Inteprets the command's operation type
    ///
    /// - Parameter eigthByte: UInt8 value that is the 8th byte in the array representing the command's operation type
    /// - Returns : ConfigOperationType? enumerator value representing the operation type (assign value or offset value).
    private func intepretOperationType(eigthByte : UInt8) -> ConfigOperationType? {
        switch eigthByte {
        case 0:
            return ConfigOperationType.assign
        case 1:
            return ConfigOperationType.offset
        default:
            return nil
        }
    }
}
