//
//  Observable.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Topias Peiponen on 26.11.2020.
//

import Foundation

protocol Observable {
    associatedtype T
    var dictionary : T { get set }
    var observers : [Observer] { get set }
    
    func addObserver(observer : Observer)
    func removeObserver(observer: Observer)
    func notifyAllObservers<T>(with newValue: T)
}
