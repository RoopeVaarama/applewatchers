//
//  Observer.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Topias Peiponen on 26.11.2020.
//

import Foundation

protocol Observer {
    var id : Int { get }
    func update<T>(with newValue: T)
}
