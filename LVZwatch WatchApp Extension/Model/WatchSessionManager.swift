//
//  WatchSessionManager.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Topias Peiponen on 13.11.2020.
//

import Foundation
import WatchConnectivity

/// Singleton for managing Watch Sessions (WCSession)
///
/// A singleton is required so that the same instance of the session can be accessed from all watch controllers.
class WatchSessionManager : NSObject, WCSessionDelegate{
    
    private var _dictionary : [String : Any?] = [:]
    private var _firstPreset : [ String : Any?] =
        [CameraConfig.presetName.rawValue :  nil]
    private var _secondPreset : [String : Any?] =
        [CameraConfig.presetName.rawValue :  nil]
    private var _thirdPreset : [String : Any?] =
        [CameraConfig.presetName.rawValue :  nil]
    private var _fourthPreset : [String : Any?] =
        [CameraConfig.presetName.rawValue :  nil]
    private var _observers : [Observer] = []
    private var _recordingStatus : Bool = false
    
    /// Instantiate the Session Manager singleton
    static let sharedManager = WatchSessionManager()
    
    private override init() {
        super.init()
    }
    
    /// Before declaring session, check if WCSession is supported
    private let session : WCSession? = WCSession.isSupported() ? WCSession.default : nil
    
    /// Initialize the session
    func startSession() {
        session?.delegate = self
        session?.activate()
    }
    
    /// Send a message through the current session
    ///
    /// Universal function for passing all values that are to be changed in the peripheral to the phone. Business logic is handled on the phone side.
    /// - Parameter message: Format is [String : Any]. For example ["ISO", 400]
    func sendMessage(message : [String: Any]) {
        if (session != nil) {
            DispatchQueue.global(qos:.background).sync{
                self.session?.sendMessage(message, replyHandler: nil, errorHandler: nil)
            }
        }
    }
}

extension WatchSessionManager {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("activation completed with")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WatchSessionManager received message \(message)")
        let msgKey = Array(message.keys)[0]
        if (msgKey == CameraConfig.firstPreset.rawValue) {
            if let incomingFirstPresets = message[msgKey] as? [String : Any?] {
                var newPreset : [String : Any?] = [:]
                for pair in incomingFirstPresets {
                    if (pair.key == CameraConfig.presetName.rawValue && pair.value != nil) {
                        newPreset[pair.key] = pair.value
                    }
                }
                firstPreset = newPreset
            }
        } else if (msgKey == CameraConfig.secondPreset.rawValue) {
            let msgKey = Array(message.keys)[0]
            if let incomingSecondPresets = message[msgKey] as? [String : Any] {
                let presetKeys = Array(incomingSecondPresets.keys)
                var newPreset : [String : Any?] = [:]
                for value in presetKeys {
                    newPreset[value] = incomingSecondPresets[value]
                }
                secondPreset = newPreset
            }
        } else if (msgKey == CameraConfig.thirdPreset.rawValue) {
            let msgKey = Array(message.keys)[0]
            if let incomingThirdPresets = message[msgKey] as? [String : Any] {
                let presetKeys = Array(incomingThirdPresets.keys)
                var newPreset : [String : Any?] = [:]
                for value in presetKeys {
                    newPreset[value] = incomingThirdPresets[value]
                }
                thirdPreset = newPreset
            }
        } else if (msgKey == CameraConfig.fourthPreset.rawValue) {
            let msgKey = Array(message.keys)[0]
            if let incomingFourthPresets = message[msgKey] as? [String : Any] {
                let presetKeys = Array(incomingFourthPresets.keys)
                var newPreset : [String : Any?] = [:]
                for value in presetKeys {
                    newPreset[value] = incomingFourthPresets[value]
                }
                fourthPreset = newPreset
            }
        } else if (msgKey == CameraConfig.record.rawValue) {
            if let status = message[msgKey] as? Bool {
                recordingStatus = status
            }
        } else {
            dictionary[msgKey] = message[msgKey]
        }
    }
}

extension WatchSessionManager : Observable {
    var dictionary: [String : Any?] {
        get {
            return _dictionary
        }
        set {
            _dictionary = newValue
            self.notifyAllObservers(with: newValue)
        }
    }
    
    var firstPreset : [String : Any?] {
        get {
            return _firstPreset
        }
        set {
            _firstPreset = newValue
            self.notifyAllObservers(with: newValue)
        }
    }
    
    var secondPreset : [String : Any?] {
        get {
            return _secondPreset
        }
        set {
            _secondPreset = newValue
            self.notifyAllObservers(with: newValue)
        }
    }
    
    var thirdPreset : [String : Any?] {
        get {
            return _thirdPreset
        }
        set {
            _thirdPreset = newValue
            self.notifyAllObservers(with: newValue)
        }
    }
    
    var fourthPreset : [String : Any?] {
        get {
            return _fourthPreset
        }
        set {
            _fourthPreset = newValue
            self.notifyAllObservers(with: newValue)
        }
    }
    
    var observers: [Observer] {
        get {
            return _observers
        }
        set {
            _observers = newValue
        }
    }
    
    var recordingStatus : Bool {
        get {
            return _recordingStatus
        }
        set {
            _recordingStatus = newValue
            self.notifyAllObservers(with: newValue)
        }
    }
    
    typealias T = [String : Any?]
    
    func addObserver(observer: Observer) {
        observers.append(observer)
        notifyAllObservers(with: observers)
    }
    func removeObserver(observer: Observer) {
        observers = observers.filter{$0.id != observer.id}
    }
    func notifyAllObservers<T>(with newValue: T) {
        print("Notifying observers")
        print("dictionary \(dictionary)")
        for observer in observers {
            print(observer)
            if (observer.id == 8) {
                let allPresets = [CameraConfig.preset.rawValue : [[CameraConfig.firstPreset.rawValue : firstPreset], [CameraConfig.secondPreset.rawValue : secondPreset], [CameraConfig.thirdPreset.rawValue : thirdPreset],
                    [CameraConfig.fourthPreset.rawValue : fourthPreset]]
                ]
                print("WatchSessionManaged notifying with presets \(allPresets)")
                observer.update(with: allPresets)
                observer.update(with: recordingStatus)
            } else if (observer.id != 8) {
                observer.update(with: recordingStatus)
                observer.update(with: dictionary)
            } else {
                observer.update(with: dictionary)
            }
        }
    }
    func setPreset(preset : [String : Any?]) {
        let presetKeyArray = Array(preset.keys)
        for key in presetKeyArray {
            if let value = preset[key] {
                dictionary[key] = value
            }
        }
    }
}
