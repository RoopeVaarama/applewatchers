//
//  ValuesController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Roope Vaarama on 4.12.2020.
//
import WatchKit
import Foundation

class ValuesController: WKInterfaceController, Observer {
    var id: Int = 9
    
    func update<T>(with newValue: T) {
        <#code#>
    }
    
    
    override func awake(withContext context: Any?) {
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }

    override func willActivate() {
    // This method is called when watch view controller is about to be visible to user
    }

    override func didDeactivate() {
    // This method is called when watch view controller is no longer visible
    WatchSessionManager.sharedManager.removeObserver(observer: self)
    super.didDeactivate()
    }
}
