//
//  ValuesInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 10.11.2020.
//

import UIKit
import WatchKit

class ValuesInterfaceController: WKInterfaceController {
    
    @IBOutlet var isoValueLabel: WKInterfaceLabel!
    @IBOutlet var irisValueLabel: WKInterfaceLabel!
    @IBOutlet var shutterValueLabel: WKInterfaceLabel!
    @IBOutlet var whiteBalanceValueLabel: WKInterfaceLabel!
    @IBOutlet var focusValueLabel: WKInterfaceLabel!
    
    
    override func awake(withContext context: Any?) {
        // Configure interface objects here.
        //Update camera values to labels here
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }

}
