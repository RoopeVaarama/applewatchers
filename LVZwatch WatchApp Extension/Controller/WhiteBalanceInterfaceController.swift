//
//  WhiteBalanceInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 10.11.2020.
//

import Foundation
import WatchKit

class WhiteBalanceInterfaceController: WKInterfaceController, WKCrownDelegate, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        // Configure interface objects here.
        crownSequencer.delegate = self
        whiteBalanceLabel.setText("2500K")
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }
    
    override func willActivate() {
        crownSequencer.focus()
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        WatchSessionManager.sharedManager.dictionary[CameraConfig.pureWhiteBalance.rawValue] = wbValue
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    
    //MARK: Properties
    @IBOutlet weak var whiteBalanceSlider: WKInterfaceSlider!
    @IBOutlet weak var whiteBalanceLabel: WKInterfaceLabel!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    var crownAccumulator = 0.0
    var id: Int = 4
    
    //MARK: White Balance change functions
    var wbValue : Int16 = 0 {
        didSet {
            let sliderVal = (Float(wbValue) - 2500) / 50
            print(sliderVal)
            whiteBalanceSlider.setValue(sliderVal)
            whiteBalanceLabel.setText("\(wbValue)K")
            wbSliderValue = sliderVal
        }
    }
    var wbSliderValue : Float = 0 {
        didSet {
            let futureWBValue = Int16((wbSliderValue * 50) + 2500)
            
            if (futureWBValue != wbValue) {
                wbValue = futureWBValue
                WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.pureWhiteBalance.rawValue : wbValue])
            }
        }
    }
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("WhitebalanceinterfaceController \(newValue)")
        print("wbvalue: \(wbValue)")
        let newValueDict = newValue as? [String : Any?]
        if let newWBValue = newValueDict?[CameraConfig.pureWhiteBalance.rawValue] as? Int16 {
            print(newWBValue)
            wbValue = newWBValue
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }

    //MARK: Slider functionalities
    //This function listens to the slider changes, for now it always updates the label when value is changed.
    @IBAction func sliderDidChange(_ value: Float) {
        wbSliderValue = value
    }
    
    //MARK: Crown functionalities
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        print("\(rotationalDelta)")
        
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.1  {
            if(wbSliderValue < 150){
                wbSliderValue += 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            if(wbSliderValue > 0){
                wbSliderValue -= 1
            }
            crownAccumulator = 0.0
        }
    }
}
