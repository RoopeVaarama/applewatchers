//
//  IrisInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 9.11.2020.
//

import Foundation
import WatchKit
import WatchConnectivity

class IrisInterfaceController: WKInterfaceController, WKCrownDelegate, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
        currentApertureValueLabel.setText("f 6.0")
        print("adding observer")
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }
    
    override func willActivate() {
        super.willActivate()
        crownSequencer.focus()
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        WatchSessionManager.sharedManager.dictionary[CameraConfig.apertureFstop.rawValue] = apertureValue
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    
    //MARK: Properties
    @IBOutlet var apertureSlider: WKInterfaceSlider!
    @IBOutlet var currentApertureValueLabel: WKInterfaceLabel!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    var crownAccumulator = 0.0
    let apertureValueConverter = ApertureValueConverter()
    var id: Int = 1
    
    //MARK: Aperture change functions
    var apertureValue : Double = 0.0 {
        didSet {
            print(apertureValue)
            let fNumber = apertureValueConverter.convertApertureValueToFNumber(apertureValue: apertureValue)
            apertureSlider.setValue(Float(apertureValue * 10))
            currentApertureValueLabel.setText(fNumber)
            apertureSliderValue = Float(apertureValue * 10)
        }
    }
    var apertureSliderValue : Float = 0 {
        didSet {
            print("aperture slider val \(apertureSliderValue)")
            let futureApertureValue = Double(apertureSliderValue) / 10
            if (futureApertureValue != apertureValue) {
                apertureValue = futureApertureValue
                WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.apertureFstop.rawValue : apertureValue])
            }
        }
    }
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("IrisInterfaceController: \(newValue)")
        let newValueDict = newValue as? [String : Any?]
        if let newValue = newValueDict?[CameraConfig.apertureFstop.rawValue] as? Double {
            apertureValue = newValue
        } 
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }
    
    //MARK: Slider functionalities
    //This function listens to the slider changes, for now it always updates the label when value is changed.
    @IBAction func sliderDidChange(_ value: Float) {
        apertureSliderValue = value
    }
    
    //MARK: Crown functionalities
    
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.1  {
            if(apertureSliderValue < 159){
                apertureSliderValue += 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            if(apertureSliderValue > -10){
                apertureSliderValue -= 1
            }
            crownAccumulator = 0.0
        }
    }
}
