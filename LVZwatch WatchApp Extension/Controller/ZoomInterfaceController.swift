//
//  ZoomInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 7.12.2020.
//

import Foundation
import WatchKit

class ZoomInterfaceController: WKInterfaceController, WKCrownDelegate, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
        zoomValueLabel.setText("0")
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }
    
    override func willActivate() {
        super.willActivate()
        crownSequencer.focus()
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        //WatchSessionManager.sharedManager.dictionary[CameraConfig.contZoom.rawValue] = zoomSpeedValue
        WatchSessionManager.sharedManager.dictionary[CameraConfig.levelZoom.rawValue] = zoomValue
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    
    //MARK: Properties

  
    //@IBOutlet weak var speedSlider: WKInterfaceSlider!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    @IBOutlet weak var zoomValueSlider: WKInterfaceSlider!
    @IBOutlet weak var zoomValueLabel: WKInterfaceLabel!
    var crownAccumulator = 0.0
    var id: Int = 7
    let changingVelocityValue = 0.523599
    var crownRotationalDelta = 0.0
    
    //MARK: Zoom variable observers
    /*var zoomSpeedValue : Double = 0.0 {
        didSet {
            speedSlider.setValue(Float(zoomSpeedValue) * 100)
        }
    }
    var zoomSpeedSlider : Float = 0 {
        didSet {
            let futureZoomSpeed = Double(zoomSpeedSlider) / 100
            if (futureZoomSpeed != zoomSpeedValue) {
                zoomSpeedValue = futureZoomSpeed
                WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.contZoom.rawValue : zoomSpeedValue])
            }
        }
    }*/
    var zoomValue : Double = 0 {
        didSet {
            let zoomValueToSliderLevel = Float(zoomValue) * 1000
            zoomValueSlider.setValue(zoomValueToSliderLevel)
            zoomValueLabel.setText(String(Int(zoomValueToSliderLevel)))
            zoomSliderValue = zoomValueToSliderLevel
        }
    }
    var zoomSliderValue : Float = 0 {
        didSet {
            let futureZoomValue = Double(zoomSliderValue) / 1000
            if (futureZoomValue != zoomValue) {
                zoomValue = futureZoomValue
                WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.levelZoom.rawValue : zoomValue])
            }
        }
    }
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("Zoominterfacecontroller: \(newValue)")
        let newValueDict = newValue as? [String : Any?]
        /*if let newZoomSpeedValue = newValueDict?[CameraConfig.contZoom.rawValue] as? Double {
            zoomSpeedValue = newZoomSpeedValue
        } else {
            return
        }*/
        
        if let newZoomLevelValue = newValueDict?[CameraConfig.levelZoom.rawValue] as? Double {
            zoomValue = newZoomLevelValue
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }

    //MARK: Slider functionalities
    //This function listens to the slider changes, for now it always updates the label when value is changed.
    
    
    /*@IBAction func zoomSpeedSliderDidChange(_ value: Float) {
        zoomSpeedSlider = roundf(value)
    }*/
    @IBAction func zoomValueSliderDidChange(_ value: Float) {
        zoomSliderValue = roundf(value)
    }
    
    //MARK: Crown functionalities
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.01 && crownAccumulator <= 0.1 {
            if(zoomSliderValue < 1000){
                zoomSliderValue += 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator > 0.1 && crownAccumulator <= 0.15 {
            if(zoomSliderValue < 990){
                zoomSliderValue += 10
            }
            crownAccumulator = 0.0
        } else if crownAccumulator > 0.15 {
            if(zoomSliderValue < 900){
                zoomSliderValue += 100
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.01 && crownAccumulator >= -0.1 {
            if(zoomSliderValue > 0){
                zoomSliderValue -= 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 && crownAccumulator >= -0.15 {
            if(zoomSliderValue > 10){
                zoomSliderValue -= 10
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.15 {
            if(zoomSliderValue > 100){
                zoomSliderValue -= 100
            }
            crownAccumulator = 0.0
        }
    }
}

