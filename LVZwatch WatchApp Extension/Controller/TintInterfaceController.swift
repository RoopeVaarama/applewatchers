//
//  TintInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Roope Vaarama on 5.12.2020.
//

import Foundation
import WatchKit

class TintInterfaceController: WKInterfaceController, Observer, WKCrownDelegate {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    var tintValue : Int16 = 0 {
        didSet {
            print("Tint value: \(tintValue)")
            TintValueSlider.setValue(Float(tintValue))
            TintValueLabel.setText(String(tintValue))
            tintSliderValue = Float(tintValue)
        }
    }
    var tintSliderValue : Float = 0 {
        didSet {
            let futureTintValue = Int16(tintSliderValue)
            if (futureTintValue != tintValue) {
                tintValue = futureTintValue
                WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.tint.rawValue : tintValue])
            }
        }
    }

    //MARK: Override funtions
    override func awake(withContext context: Any?) {
        TintValueLabel.setText("0")
        WatchSessionManager.sharedManager.addObserver(observer: self)
        crownSequencer.delegate = self
    }

    override func willActivate() {
        crownSequencer.focus()
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }

    }

    override func didDeactivate() {
        
        WatchSessionManager.sharedManager.removeObserver(observer: self)
        WatchSessionManager.sharedManager.dictionary[CameraConfig.tint.rawValue] = tintValue
        // This method is called when watch view controller is no longer visible
    }

    //MARK: Properties
    let id : Int = 10
    @IBOutlet var TintValueSlider: WKInterfaceSlider!
    @IBOutlet var TintValueLabel: WKInterfaceLabel!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    var crownAccumulator = 0.0
    
    @IBAction func tintSliderValueChanged(_ value: Float) {
        tintSliderValue = value
    }
    
    //MARK: Update function
    func update<T>(with newValue: T) {
        print("TINTINTERFACECONTROLLER: \(newValue)")
        if let newValueDict = newValue as? [String : Any?] {
            if let tint = newValueDict[CameraConfig.tint.rawValue] as? Int16 {
                tintValue = tint
            }
            
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    
    }
    
    //MARK: Crown functionalities
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        print("\(rotationalDelta)")
        
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.1  {
            if(tintSliderValue < 50){
                tintSliderValue += 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            if(tintSliderValue > -50){
                tintSliderValue -= 1
            }
            crownAccumulator = 0.0
        }
    }
}
