//
//  PresetsController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Roope Vaarama on 3.12.2020.
//
import WatchKit
import Foundation

class PresetsController: WKInterfaceController, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override funtions
    override func awake(withContext context: Any?) {
        WatchSessionManager.sharedManager.addObserver(observer: self)
        
        firstPresetLabel.setEnabled(false)
        secondPresetLabel.setEnabled(false)
        thirdPresetLabel.setEnabled(false)
        fourthPresetLabel.setEnabled(false)
        
        let firstPresetTitle = WatchSessionManager.sharedManager.firstPreset[CameraConfig.presetName.rawValue] as? String
        let secondPresetTitle = WatchSessionManager.sharedManager.secondPreset[CameraConfig.presetName.rawValue] as? String
        let thirdPresetTitle = WatchSessionManager.sharedManager.thirdPreset[CameraConfig.presetName.rawValue] as? String
        let fourthPresetTitle = WatchSessionManager.sharedManager.fourthPreset[CameraConfig.presetName.rawValue] as? String
        
        print(firstPresetTitle ?? "preset 1")
        firstPresetLabel.setTitle(firstPresetTitle ?? "Preset 1")
        secondPresetLabel.setTitle(secondPresetTitle ?? "Preset 2")
        thirdPresetLabel.setTitle(thirdPresetTitle ?? "Preset 3")
        fourthPresetLabel.setTitle(fourthPresetTitle ?? "Preset 4")
    }

    override func willActivate() {
        WatchSessionManager.sharedManager.addObserver(observer: self)
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
    }

    override func didDeactivate() {
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    
    //MARK: Properties
    let id : Int = 8
    @IBOutlet weak var firstPresetLabel: WKInterfaceButton!
    @IBOutlet weak var secondPresetLabel: WKInterfaceButton!
    @IBOutlet weak var thirdPresetLabel: WKInterfaceButton!
    @IBOutlet weak var fourthPresetLabel: WKInterfaceButton!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    
    var firstPresetOn = false {
        didSet {
            if (firstPresetOn) {
                firstPresetLabel.setEnabled(true)
            } else {
                firstPresetLabel.setEnabled(false)
            }
        }
    }
    var secondPresetOn = false {
        didSet {
            if (secondPresetOn) {
                secondPresetLabel.setEnabled(true)
            } else {
                secondPresetLabel.setEnabled(false)
            }
        }
    }
    var thirdPresetOn = false {
        didSet {
            if (thirdPresetOn) {
                thirdPresetLabel.setEnabled(true)
            } else {
                thirdPresetLabel.setEnabled(false)
            }
        }
    }
    var fourthPresetOn = false {
        didSet {
            if (fourthPresetOn) {
                fourthPresetLabel.setEnabled(true)
            } else {
                fourthPresetLabel.setEnabled(false)
            }
        }
    }
    
    var firstPresetName : String = "" {
        didSet {
            print("Set label here \(firstPresetName)")
            firstPresetLabel.setTitle(firstPresetName)
        }
    }
    var secondPresetName : String = "" {
        didSet {
            print("Set label here \(secondPresetName)")
            secondPresetLabel.setTitle(secondPresetName)
        }
    }
    var thirdPresetName : String = "" {
        didSet {
            print("Set label here \(thirdPresetName)")
            thirdPresetLabel.setTitle(thirdPresetName)
        }
    }
    var fourthPresetName : String = "" {
        didSet {
            print("Set label here \(fourthPresetName)")
            fourthPresetLabel.setTitle(fourthPresetName)
        }
    }
    
    @IBAction func pressedFirstPreset() {
        print("pressed dis shit first")
        WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.firstPreset.rawValue : true])
    }
    @IBAction func pressedSecondPreset() {
        WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.secondPreset.rawValue : true])
    }
    @IBAction func pressedThirdPreset() {
        WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.thirdPreset.rawValue : true])
    }
    @IBAction func pressedFourthPreset() {
        WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.fourthPreset.rawValue : true])
    }
    

    //MARK: Update function
    func update<T>(with newValue: T) {
        print("PresetsController: \(newValue)")
        
        if let newPresets = newValue as? [String : [[String : [String : Any?]]]] {
            let presetsKey = Array(newPresets.keys)[0]
            if let presetsArray = newPresets[presetsKey] {
                print("Presets array: \(presetsArray)")
                
                for preset in presetsArray {
                
                for key in preset.keys {
                    print("Key in collection: \(preset.keys)")
                    if let presetsPair = preset[key] {
                        
                        for pair in presetsPair {
                            print("Pair: \(pair)")
                            if (pair.key == CameraConfig.presetName.rawValue && pair.value != nil) {
                                
                                switch key {
                                case CameraConfig.firstPreset.rawValue:
                                    firstPresetName = pair.value as? String ?? "Preset 1"
                                    firstPresetOn = true
                                case CameraConfig.secondPreset.rawValue:
                                    secondPresetName = pair.value as? String ?? "Preset 2"
                                    secondPresetOn = true
                                case CameraConfig.thirdPreset.rawValue:
                                    thirdPresetName = pair.value as? String ?? "Preset 3"
                                    thirdPresetOn = true
                                case CameraConfig.fourthPreset.rawValue:
                                    fourthPresetName = pair.value as? String ?? "Preset 4"
                                    fourthPresetOn = true
                                default:
                                    print("Could not find presets")
                                }
                            }
                        }
                    }
                    
                }
                }
            }
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }
    
}
