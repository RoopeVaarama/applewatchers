//
//  RecordScreenInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 10.11.2020.
//

import UIKit
import Foundation
import WatchKit
import WatchConnectivity

class RecordScreenInterfaceController: WKInterfaceController, Observer {
    var id: Int = 6
    
    func update<T>(with newValue: T) {
        print("Recordscreeninterfacecontroller \(newValue)")
    }
    
    @IBOutlet weak var startRecordButton: WKInterfaceButton!
    var isRecording = false
    @IBOutlet weak var recordedTimeTimer: WKInterfaceTimer!
    
    override func awake(withContext context: Any?) {
        // Configure interface objects here.
        WatchSessionManager.sharedManager.startSession()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    
    //MARK: Functions for controlling the record button
    
    @IBAction func recordButtonPressed() {
        
        if (isRecording == false ) {
            isRecording = true
            let string = [CameraConfig.startRecord.rawValue : "true"]
            WatchSessionManager.sharedManager.sendMessage(message: string)
            startRecordButton.setBackgroundColor(UIColor.red)
            recordedTimeTimer.start()
        } else if (isRecording == true) {
            isRecording = false
            let string = [CameraConfig.stopRecord.rawValue : "false"]
            WatchSessionManager.sharedManager.sendMessage(message: string)
            startRecordButton.setBackgroundColor(UIColor.orange)
            recordedTimeTimer.stop()
        }
        
    }
}
