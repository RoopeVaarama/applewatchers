//
//  InterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Roope Vaarama on 27.10.2020.
//

import WatchKit
import Foundation
import CoreBluetooth
import WatchConnectivity


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var testButtonRec: WKInterfaceButton!
    let session = WCSession.default
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        session.delegate = self
        session.activate()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        

    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    @IBAction func testButton() {
        let string = "autoFocus"
        print(string)
        guard let data = Data(base64Encoded: string) else { return }
        session.sendMessageData(data, replyHandler: nil, errorHandler: nil)
    }
}

extension InterfaceController : WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print(activationState, error)
    }
    func session(_ session: WCSession, didReceiveMessageData messageData: Data) {
        print(messageData)
    }
    
}

