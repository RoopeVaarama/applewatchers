//
//  InterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Roope Vaarama on 27.10.2020.
//

import WatchKit
import Foundation
import CoreBluetooth
import WatchConnectivity
import UserNotifications


class InterfaceController: WKInterfaceController, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordButton.setBackgroundColor(UIColor.red)
            } else {
                recordButton.setBackgroundColor(UIColor.lightGray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        WatchSessionManager.sharedManager.startSession()
        recordStatus = WatchSessionManager.sharedManager.recordingStatus
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }
    
    override func willActivate() {
        WatchSessionManager.sharedManager.addObserver(observer: self)
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        WatchSessionManager.sharedManager.removeObserver(observer: self)
        // This method is called when watch view controller is no longer visible
    }
    
    //MARK: Properties
    var id: Int = 0
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("Interface controller \(newValue)")
        
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }
    //MARK: Record button functions
    @IBOutlet weak var recordButton: WKInterfaceButton!
    
    @IBAction func recordButtonPressed() {
            if (!recordStatus) {
                recordStatus = true
                WatchSessionManager.sharedManager.recordingStatus = true
                let string = [CameraConfig.startRecord.rawValue : true]
                WatchSessionManager.sharedManager.sendMessage(message: string)
                print("recording set to true")
            } else if (recordStatus) {
                recordStatus = false
                WatchSessionManager.sharedManager.recordingStatus = false
                let string = [CameraConfig.stopRecord.rawValue : true]
                WatchSessionManager.sharedManager.sendMessage(message: string)
        }
        
    }
}
