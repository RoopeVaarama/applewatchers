//
//  TableInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Roope Vaarama on 16.11.2020.
//

import WatchKit
import Foundation


class TableInterfaceController: WKInterfaceController, Observer {
    var id: Int = 7
    
    func update<T>(with newValue: T) {
        print("TableInterfaceController \(newValue)")
        if let newDict = newValue as? [String : Any?] {
            let newDictKeys = Array(newDict.keys)
            for key in newDictKeys {
                print(newDict[key])
            }
        }
    }
    

    //MARK: Properts
    @IBOutlet var menuTable: WKInterfaceTable!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    let tableItems = ["White Balance", "ISO", "Shutter", "Aperture", "Focus", "Values", "About"]
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        menuTable.setNumberOfRows(tableItems.count, withRowType: "MenuRow")
        
        for(index, item) in tableItems.enumerated(){
            guard let row = menuTable.rowController(at: index) as? MenuRow else{
                continue
            }
            row.textLabel.setText(item)
        }
        self.becomeCurrentPage()
        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        WatchSessionManager.sharedManager.removeObserver(observer: self)
        super.didDeactivate()
        
    }

    //MARK: Table selected
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        print("\(tableItems[rowIndex])")
        pushController(withName: tableItems[rowIndex], context: (Any).self)
    }
}
