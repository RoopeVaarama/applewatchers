//
//  FocusInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 10.11.2020.
//

import Foundation
import WatchKit

class FocusInterfaceController: WKInterfaceController, WKCrownDelegate, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
        focusValueLabel.setText("0")
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }
    
    override func willActivate() {
        super.willActivate()
        crownSequencer.focus()
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        WatchSessionManager.sharedManager.dictionary[CameraConfig.absoluteFocus.rawValue] = absoluteFocus
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    
    //MARK: Properties
    @IBOutlet weak var focusValueSlider: WKInterfaceSlider!
    @IBOutlet weak var focusValueLabel: WKInterfaceLabel!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    var crownAccumulator = 0.0
    var id: Int = 5
    let changingVelocityValue = 0.523599
    var crownRotationalDelta = 0.0
    
    //MARK: Focus change functions
    var absoluteFocus : Double = 0.0 {
        didSet {
            let focSliderValue = Float(absoluteFocus * 1000)
            focusValueSlider.setValue(focSliderValue)
            focusValueLabel.setText(String(format: "%.0f", focSliderValue))
            absoluteFocusSlider = focSliderValue
        }
    }
    var absoluteFocusSlider : Float = 0 {
        didSet {
            var futureAbsFocus : Double = Double(absoluteFocusSlider / 1000)
            futureAbsFocus = Double(round(1000*futureAbsFocus)/1000)
            print("Setting slider abs focus: \(futureAbsFocus)")
            if (futureAbsFocus != absoluteFocus) {
                absoluteFocus = futureAbsFocus
                WatchSessionManager.sharedManager.sendMessage(message:  [CameraConfig.absoluteFocus.rawValue : absoluteFocus])
            }
        }
    }
    
    @IBAction func AutoFocusButton() {
        WatchSessionManager.sharedManager.sendMessage(message:  [CameraConfig.autoFocus.rawValue : true])
    }
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("Focusinterfacecontroller: \(newValue)")
        let newValueDict = newValue as? [String : Any?]
        if let newFocusValue = newValueDict?[CameraConfig.absoluteFocus.rawValue] as? Double {
            absoluteFocus = newFocusValue
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }

    //MARK: Slider functionalities
    //This function listens to the slider changes, for now it always updates the label when value is changed.
    @IBAction func sliderDidChange(_ value: Float) {
        absoluteFocusSlider = value
    }
    
    //MARK: Crown functionalities
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.01 && crownAccumulator <= 0.1 {
            if(absoluteFocusSlider < 1000){
                absoluteFocusSlider += 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator > 0.1 && crownAccumulator <= 0.20 {
            if(absoluteFocusSlider < 990){
                absoluteFocusSlider += 10
            }
            crownAccumulator = 0.0
        } else if crownAccumulator > 0.20 {
            if(absoluteFocusSlider < 900){
                absoluteFocusSlider += 100
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.01 && crownAccumulator >= -0.1 {
            if(absoluteFocusSlider > 0){
                absoluteFocusSlider -= 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 && crownAccumulator >= -0.20 {
            if(absoluteFocusSlider > 10){
                absoluteFocusSlider -= 10
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.20 {
            if(absoluteFocusSlider > 100){
                absoluteFocusSlider -= 100
            }
            crownAccumulator = 0.0
        }
    }
}
