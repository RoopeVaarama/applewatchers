//
//  IsoInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 10.11.2020.
//

import Foundation
import WatchKit
import WatchConnectivity

class IsoInterfaceController: WKInterfaceController, WKCrownDelegate, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
        
        IsoValueLabel.setText("100")
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }
    
    override func willActivate() {
        super.willActivate()
        crownSequencer.focus()
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        print("Removing ISOinterfacecontroller from observing")
        WatchSessionManager.sharedManager.dictionary[CameraConfig.ISO.rawValue] = isoValue
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    
    //MARK: Properties
    @IBOutlet var IsoValueSlider: WKInterfaceSlider!
    @IBOutlet var IsoValueLabel: WKInterfaceLabel!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    var crownAccumulator = 0.0
    var id: Int = 2
    
    //MARK: ISO change functions
    var isoValue : Int32 = 0 {
        didSet {
            switch isoValue {
            case 100:
                IsoValueSlider.setValue(0)
                isoSliderValue = 0.0
            case 200:
                IsoValueSlider.setValue(1)
                isoSliderValue = 1.0
            case 400:
                IsoValueSlider.setValue(2)
                isoSliderValue = 2.0
            case 800:
                IsoValueSlider.setValue(3)
                isoSliderValue = 3.0
            case 1600:
                IsoValueSlider.setValue(4)
                isoSliderValue = 4.0
            case 3200:
                IsoValueSlider.setValue(5)
                isoSliderValue = 5.0
            case 6400:
                IsoValueSlider.setValue(6)
                isoSliderValue = 6.0
            default:
                IsoValueSlider.setValue(0)
                print("Invalid ISO value")
            }
            IsoValueLabel.setText(String(isoValue))
        }
    }
    var isoSliderValue : Float = 0 {
        didSet {
            var futureIsoValue : Int32? = nil
            switch isoSliderValue{
                case 0:
                    futureIsoValue = Int32(100)
                case 1:
                    futureIsoValue = Int32(200)
                case 2:
                    futureIsoValue = Int32(400)
                case 3:
                    futureIsoValue = Int32(800)
                case 4:
                    futureIsoValue = Int32(1600)
                case 5:
                    futureIsoValue = Int32(3200)
                case 6:
                    futureIsoValue = Int32(6400)
                default:
                    print("Could not change slider value")
            }
            /// If current value doesn't match set value, change the current value
            if (futureIsoValue != isoValue && futureIsoValue != nil) {
                print("ISO changed")
                isoValue = futureIsoValue ?? isoValue
                WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.ISO.rawValue : isoValue])
            }
        }
    }
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("IsoInterfaceController: \(newValue)")
        let newValueDict = newValue as? [String : Any?]
        if let newIsoValue = newValueDict?[CameraConfig.ISO.rawValue] as? Int32 {
            isoValue = newIsoValue
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }

    //MARK: Slider functionalities
    //This function listens to the slider changes, for now it always updates the label when value is changed.
    @IBAction func sliderDidChange(_ value: Float) {
        isoSliderValue = value
    }
    
    //MARK: Crown functionalities
    
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.1  {
            if(isoSliderValue < 6){
                isoSliderValue += 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            if(isoSliderValue > 0){
                isoSliderValue -= 1
            }
            crownAccumulator = 0.0
        }
    }
}
