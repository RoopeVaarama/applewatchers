//
//  ShutterInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by iosdev on 10.11.2020.
//

import Foundation
import WatchKit
import WatchConnectivity

class ShutterInterfaceController: WKInterfaceController, WKCrownDelegate, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        crownSequencer.delegate = self
        ShutterValueLabel.setText("1/60")
        WatchSessionManager.sharedManager.addObserver(observer: self)
    }
    
    override func willActivate() {
        super.willActivate()
        crownSequencer.focus()
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        WatchSessionManager.sharedManager.dictionary[CameraConfig.shutterSpeed.rawValue] = shutterSpeedValue
        WatchSessionManager.sharedManager.removeObserver(observer: self)
    }
    //MARK: Properties
    @IBOutlet var ShutterValueSlider: WKInterfaceSlider!
    @IBOutlet var ShutterValueLabel: WKInterfaceLabel!
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    let shutterSpeedValueConverter : ShutterSpeedValueConverter = ShutterSpeedValueConverter()
    var crownAccumulator = 0.0
    var id: Int = 3
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("ShutterInterfaceController: \(newValue)")
        let newValueDict = newValue as? [String : Any?]
        if let newShutterSpeedValue = newValueDict?[CameraConfig.shutterSpeed.rawValue] as? Int32 {
            shutterSpeedValue = newShutterSpeedValue
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }
    
    //MARK: Shutter change functions
    var shutterSpeedValue : Int32 = 60 {
        didSet {
            let sliderValue = shutterSpeedValueConverter.convertShutterSpeedToSliderValue(shutterSpeed: shutterSpeedValue)
            if (sliderValue != nil) {
                ShutterValueSlider.setValue(sliderValue!)
                ShutterValueLabel.setText("1/\(shutterSpeedValue)")
                shutterSpeedSliderValue = sliderValue!
            }
        }
    }
    var shutterSpeedSliderValue : Float = 0 {
        didSet {
            let futureShutterSpeed = shutterSpeedValueConverter.convertSliderValueToShutterSpeed(sliderValue: shutterSpeedSliderValue)
            if (futureShutterSpeed != shutterSpeedValue && futureShutterSpeed != nil) {
                shutterSpeedValue = futureShutterSpeed!
                WatchSessionManager.sharedManager.sendMessage(message: [CameraConfig.shutterSpeed.rawValue : shutterSpeedValue])
            }
        }
    }
    
    //MARK: Slider functionalities
    //This function listens to the slider changes, for now it always updates the label when value is changed.
    @IBAction func sliderDidChange(_ value: Float) {
        shutterSpeedSliderValue = value
        }
    
    //MARK: Crown functionalities
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        print("\(rotationalDelta)")
        
        crownAccumulator += rotationalDelta
        
        if crownAccumulator > 0.1  {
            if(shutterSpeedSliderValue < 7){
                shutterSpeedSliderValue += 1
            }
            crownAccumulator = 0.0
        } else if crownAccumulator < -0.1 {
            if(shutterSpeedSliderValue > 0){
                shutterSpeedSliderValue -= 1
            }
            crownAccumulator = 0.0
        }
    }
}
