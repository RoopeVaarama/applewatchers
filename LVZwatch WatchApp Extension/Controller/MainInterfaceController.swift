//
//  TableInterfaceController.swift
//  AppleWatchers WatchApp Extension
//
//  Created by Roope Vaarama on 16.11.2020.
//

import WatchKit
import Foundation


class MainInterfaceController: WKInterfaceController, Observer {
    //MARK: Recording status
    var recordStatus : Bool = false {
        didSet {
            if (recordStatus == true) {
                recordingStatus.setText("REC")
                recordingStatus.setTextColor(UIColor.red)
            } else {
                recordingStatus.setText("")
                recordingStatus.setTextColor(UIColor.gray)
            }
        }
    }
    
    //MARK: Override functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        WatchSessionManager.sharedManager.addObserver(observer: self)
        self.becomeCurrentPage()
        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        WatchSessionManager.sharedManager.addObserver(observer: self)
        if (WatchSessionManager.sharedManager.recordingStatus == true) {
            recordStatus = true
        } else {
            recordStatus = false
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        WatchSessionManager.sharedManager.removeObserver(observer: self)
        super.didDeactivate()
        
    }
    
    //MARK: Properties
    @IBOutlet weak var recordingStatus: WKInterfaceLabel!
    var id: Int = 7
    @IBOutlet var WhiteBalanceValueLabel: WKInterfaceLabel!
    @IBOutlet var IsoValueLabel: WKInterfaceLabel!
    @IBOutlet var ShutterValueLabel: WKInterfaceLabel!
    @IBOutlet var ApertureValueLabel: WKInterfaceLabel!
    @IBOutlet var FocusValueLabel: WKInterfaceLabel!
    @IBOutlet var TintValueLabel: WKInterfaceLabel!
    @IBOutlet weak var ZoomValueLabel: WKInterfaceLabel!
    let apertureValueConverter = ApertureValueConverter()
    
    //MARK: Update
    func update<T>(with newValue: T) {
        print("TableInterfaceController \(newValue)")
        if let newDict = newValue as? [String : Any?] {
            let newDictKeys = Array(newDict.keys)
            for key in newDictKeys {
                print(newDict[key] as Any)
                switch key {
                
                    case CameraConfig.startRecord.rawValue:
                        if let value = newDict[key] as? Bool {
                            recordStatus = true
                            print(value)
                        }
                    
                    case CameraConfig.stopRecord.rawValue:
                        if let value = newDict[key] as? Bool {
                            recordStatus = false
                            print(value)
                        }
                    
                    case CameraConfig.pureWhiteBalance.rawValue:
                        if let value = newDict[key] as? Int16 {
                            WhiteBalanceValueLabel.setText(String("\(value)K"))
                            TintValueLabel.setText("")
                        }
                    
                    case CameraConfig.ISO.rawValue:
                        if let value = newDict[key] as? Int16 {
                            IsoValueLabel.setText(String(value))
                        }
                    
                    case CameraConfig.shutterSpeed.rawValue:
                        if let value = newDict[key] as? Int16 {
                            ShutterValueLabel.setText(String("1/\(value)"))
                        }
                        
                    case CameraConfig.apertureFstop.rawValue:
                        if let value = newDict[key] as? Double {
                            let fNumber = apertureValueConverter.convertApertureValueToFNumber(apertureValue: value)
                            ApertureValueLabel.setText(fNumber)
                        }
                        
                    case CameraConfig.absoluteFocus.rawValue:
                        if let value = newDict[key] as? Double {
                            FocusValueLabel.setText(String(Int(value * 1000)))
                        }
                        
                    case CameraConfig.contZoom.rawValue:
                        if let value = newDict[key] as? Double {
                            ZoomValueLabel.setText(String(Int(value * 1000)))
                        }
                    case CameraConfig.levelZoom.rawValue:
                        if let value = newDict[key] as? Double {
                            ZoomValueLabel.setText(String(Int(value * 1000)))
                        }
                    case CameraConfig.tint.rawValue:
                        if let value = newDict[key] as? Int16 {
                            TintValueLabel.setText(String(value))
                        }
                
                    default:
                    print("asd")
                }
            }
            
        }
        if let status = newValue as? Bool {
            recordStatus = status
        }
    }
}
